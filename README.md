# CURSO SPRING CORE, REST, - LAMBDA EN TEST UNITARIOS -

## (****Requisito al iniciar, que el servidor de redis esté arriba.***)
### Reference Documentation
Referencer:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/#build-image)


### Prometheus
Pemite almacenar el historico de logs de monitoreo de la aplicación.
Se debe Descargar, configuar, descomprimir e iniar. Como requisito se debe habilitar prometheus en spring para que genere 
una url con los logs.

Por defecto corre en el puerto 9000, http://localhost:9090/, las expresiones nuevas dependen si se han agregado a spring.
* [Prometheus](https://prometheus.io/download/)
Dentro de la carpeta de prometheus esta el arvhivo de configuración.
  \prometheus-2.24.0\prometeus. Este archvivo se modifica para apuntar al servidor de actuator de spring, y 
  la url donde están los logs de prometheus 
* La configuración es la siguiente:
  scrape_configs:
  
```  
    # Nombre del job
    - job_name: 'prometheus'
      # Ruta
      metrics_path: /actuator/prometheus
      # Tiempo del interbalo
      scrape_interval: 5s
      # scheme defaults to 'http'.
      static_configs:
        # Lista de hosts que se monitorearan.
        - targets: ['localhost:8080']
```
### Swagger URL
[swagger-ui](http://localhost:8080/swagger-ui/#/)
### Grafana
Permite enlazar a prometheus para leer los logs y 'pintar' con el fin de monitorear los logs generando alertar. 
Para ello se debe agregar prometheus como datasource y crear el respectivo dashboard. Corre por defecto en el puerto 3000.

http://localhost:3000/?orgId=1
[grafana](https://grafana.com/grafana/download)

### Redis
Es un servicio de caché externo, el objetivo de redis es que el caché no se guarde dentro de la aplicación, 
esto permite ahorrar recursos de la máquina. 
* [Redis for win](https://github.com/microsoftarchive/redis/releases): Descargar 
* [Redis](https://redis.io/download): Descargar y construir

Corre por defecto por el puerto 6379, y la aplicación tiene dos servicios, el servidor y el cliente.
Desde el cliente podremos poner información al caché y validar que haya información en el mismo.
el siguiente comando nos permite ver que informacion hay en la region "users"
```
HGETALL users
```
### Zookeper
Viene con kafka.
Es necesario tener corriendo el Zookeper para poder iniciar Kafka.
Se crea la carpeta '\zookeper-logs' si no existe en:
```
dataDir=C:\kafka\zookeper-logs
```
Se edita el archivo 'C:\kafka\config\zookeeper.properties' y se agrega la ruta donde quedarán los logs del zookeper.
```
log.dirs=C:\kafka\zookeper-logs
```
Finalmente, se inicia el servidor llamando al binario pasando el archivo de propiedades:
```
Preferible iniciar el zookeper que trae kafka
C:\kafka\bin\windows>zookeeper-server-start.bat ..\..\config\zookeeper.properties
```

### Docker compose
Crear imagen de postgres y ejecutar un script para correr local.
> # En el director de docker-spring-core, correr el script. Ahi creamos el usuario e instalamos algunas extensiones.
> docker-compose up

### Kafka
[Kafka](https://kafka.apache.org/downloads), se debe descargar y descomprimir, para windows es necesario que la ruta se acorte, por eso se mueve a la base.
Se descomprime y renombra para que quede en:
```
C:\kafka
```

Luego se modifica las propiedades del archivo C:\kafka\config\server.properties:  
```
C:\kafka\config\server.properties
```
Podemos notar que por defecto, ya apunta al zookeper local, como indica la propiedad por defecto:
```
zookeeper.connect=localhost:2181 
```
Luego se edita la propiedad log.dirs para que paunte a la ruta donde dejaré mis logs, en este caso: C:\kafka\logs 

```
log.dirs=C:\kafka\logs
```
Finalmente iniciamos el server
```
C:\kafka\bin\windows>kafka-server-start.bat ..\..\config\server.properties
```
Comandos básicos:
```
Crear un topic:
C:\kafka\bin\windows>kafka-topics.bat --bootstrap-server localhost:9092 --create --topic jp-topic --partitions 5 --replication-factor 1
Listar los topics:
C:\kafka\bin\windows>kafka-topics.bat --bootstrap-server localhost:9092 --list
Describir un topic:
C:\kafka\bin\windows>kafka-topics.bat --bootstrap-server localhost:9092 --describe --topic jp-topic
```
La respuesta indica:
- Tiene 5 particiones, que
- Solo se tiene una replica
- Solo hay un líder el cual es quien contiene el
  topic especificados.
  
```
Iniciar un producer en la consola
kafka-console-producer.bat --topic jp-topic --bootstrap-server localhost:9092
Iniciar un consumer en ese instante:
C:\kafka\bin\windows>kafka-console-consumer.bat --topic jp-topic --bootstrap-server localhost:9092
Iniciar un consumidor con los mensajes desde el principo:
C:\kafka\bin\windows>kafka-console-consumer.bat --topic jp-topic --from-beginning --bootstrap-server localhost:9092

```

  ## CQRS
![img_1.png](img_1.png)

