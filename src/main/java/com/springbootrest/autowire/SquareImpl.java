package com.springbootrest.autowire;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SquareImpl implements Figure {
    @Value(value = "${square.side}")
    private double side;

    @Override
    public double calculateArea() {
        return side * side;
    }
}
