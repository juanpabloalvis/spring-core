package com.springbootrest.autowire;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CircleImpl implements Figure {
    @Value("${circle.radius:0}") // Si no encuentra, el valor por defecto será el que esté después del ":"
    private double radius;

    @Override
    public double calculateArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}
