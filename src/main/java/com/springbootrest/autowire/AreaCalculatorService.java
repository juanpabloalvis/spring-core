package com.springbootrest.autowire;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class AreaCalculatorService {

    private final List<Figure> figures;

    public double calculateAreas() {
        double area = 0;
        for (Figure figure :
                figures) {
            area += figure.calculateArea();
        }
        return area;

    }
}
