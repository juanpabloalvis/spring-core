package com.springbootrest.autowire;

public interface Figure {
    double calculateArea();
}
