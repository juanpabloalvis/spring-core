package com.springbootrest.profiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("prod")
public class ProdEnvironmentServiceImpl implements EnvironmentService {
    @Override
    public String getEnvironment() {
        return "Prod";
    }
}
