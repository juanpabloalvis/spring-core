package com.springbootrest.profiles;

public interface EnvironmentService {

    String getEnvironment();

}
