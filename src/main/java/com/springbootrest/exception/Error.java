package com.springbootrest.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
//@AllArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
public class Error {

    @NonNull
    private String errorCode;
    @NonNull
    private String message;
    // http status
    @NonNull
    private Integer status;
    private String url;
    private String reqMethod;

}
