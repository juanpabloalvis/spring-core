package com.springbootrest.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ErrorUtils {

    public static Error createError(final String errMsgKey,
                                    final String errorCode,
                                    final Integer httpStatusCode) {
        return Error.builder()
                .message(errMsgKey)
                .errorCode(errorCode)
                .status(httpStatusCode)
                .build();
    }
}
