package com.springbootrest.exception;

import lombok.Getter;

@Getter
public enum ErrorCode {
    // 1-0999
    GENERIC_ERROR("PROJECT-0001", "System unable to process the request"),
    HTTP_MEDIATYPE_NOT_SUPPORTED("PROJECT-0002", "Requested media type is not supported. Please use application/json or application/xml as 'Content-Type' header value"),
    HTTP_MESSAGE_NOT_WRITABLE("PROJECT-0003", "Missing 'Accept' header. Please add 'Accept' header."),
    HTTP_MEDIA_TYPE_NOT_ACCEPTABLE("PROJECT-0004", "Requested 'Accept' header value is not supported. Please use application/json or application/xml as 'Accept' value"),
    JSON_PARSE_ERROR("PROJECT-0005", "Make sure request payload should be a valid JSON object."),
    HTTP_MESSAGE_NOT_READABLE("PROJECT-0006", "Make sure request payload should be a valid JSON or XML object according to 'Content-Type'.");

    private final String errCode;
    private final String errMsgKey;

    ErrorCode(final String errCode, final String errMsgKey) {
        this.errCode = errCode;
        this.errMsgKey = errMsgKey;
    }

}
