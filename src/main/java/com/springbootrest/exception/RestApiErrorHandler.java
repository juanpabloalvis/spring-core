package com.springbootrest.exception;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

import static com.springbootrest.exception.ErrorCode.GENERIC_ERROR;
import static com.springbootrest.exception.ErrorCode.HTTP_MEDIATYPE_NOT_SUPPORTED;
import static com.springbootrest.exception.ErrorCode.HTTP_MEDIA_TYPE_NOT_ACCEPTABLE;
import static com.springbootrest.exception.ErrorCode.HTTP_MESSAGE_NOT_READABLE;
import static com.springbootrest.exception.ErrorCode.HTTP_MESSAGE_NOT_WRITABLE;
import static com.springbootrest.exception.ErrorCode.JSON_PARSE_ERROR;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE;

@ControllerAdvice
@Slf4j
public class RestApiErrorHandler {

    private final MessageSource messageSource;

    @Autowired
    public RestApiErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handleException(HttpServletRequest request, Exception ex,
                                                 Locale locale) {
        // ex.printStackTrace(); // TODO: Should be kept only for development
        Error error = Error.builder()
                .message(GENERIC_ERROR.getErrMsgKey())
                .errorCode(GENERIC_ERROR.getErrCode())
                .url(request.getRequestURL().toString())
                .status(INTERNAL_SERVER_ERROR.value())
                .reqMethod(request.getMethod())
                .build();

        return new ResponseEntity<>(error, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<Error> handleHttpMediaTypeNotSupportedException(HttpServletRequest request,
                                                                          HttpMediaTypeNotSupportedException ex,
                                                                          Locale locale) {
        ex.printStackTrace(); // TODO: Should be kept only for development
        Error error = Error.builder()
                .message(HTTP_MEDIATYPE_NOT_SUPPORTED.getErrMsgKey())
                .errorCode(HTTP_MEDIATYPE_NOT_SUPPORTED.getErrCode())
                .url(request.getRequestURL().toString())
                .status(UNSUPPORTED_MEDIA_TYPE.value())
                .reqMethod(request.getMethod())
                .build();

        log.info("HttpMediaTypeNotSupportedException :: request.getMethod(): " + request.getMethod());
        return new ResponseEntity<>(error, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMessageNotWritableException.class)
    public ResponseEntity<Error> handleHttpMessageNotWritableException(HttpServletRequest request,
                                                                       HttpMessageNotWritableException ex,
                                                                       Locale locale) {
        ex.printStackTrace(); // TODO: Should be kept only for development

        Error error = Error.builder()
                .message(HTTP_MESSAGE_NOT_WRITABLE.getErrMsgKey())
                .errorCode(HTTP_MESSAGE_NOT_WRITABLE.getErrCode())
                .url(request.getRequestURL().toString())
                .status(UNSUPPORTED_MEDIA_TYPE.value())
                .reqMethod(request.getMethod())
                .build();

        log.info("HttpMessageNotWritableException :: request.getMethod(): " + request.getMethod());
        return new ResponseEntity<>(error, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public ResponseEntity<String> handleHttpMediaTypeNotAcceptableException(HttpServletRequest request,
                                                                            HttpMediaTypeNotAcceptableException ex,
                                                                            Locale locale) {
        ex.printStackTrace(); // TODO: Should be kept only for development

        var error = Error.builder()
                .message(HTTP_MEDIA_TYPE_NOT_ACCEPTABLE.getErrMsgKey())
                .errorCode(HTTP_MEDIA_TYPE_NOT_ACCEPTABLE.getErrCode())
                .url(request.getRequestURL().toString())
                .status(UNSUPPORTED_MEDIA_TYPE.value())
                .reqMethod(request.getMethod())
                .build();
        log.info("HttpMediaTypeNotAcceptableException :: request.getMethod(): " + request.getMethod());
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            ObjectMapper mapper = new ObjectMapper();
            String errorAsJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(error);
            return new ResponseEntity<>(errorAsJson, headers, INTERNAL_SERVER_ERROR);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(HTTP_MEDIA_TYPE_NOT_ACCEPTABLE.getErrMsgKey(), INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Error> handleHttpMessageNotReadableException(HttpServletRequest request,
                                                                       HttpMessageNotReadableException ex,
                                                                       Locale locale) {
        ex.printStackTrace(); // TODO: Should be kept only for development

        Error error = Error.builder()
                .message(HTTP_MESSAGE_NOT_READABLE.getErrMsgKey())
                .errorCode(HTTP_MESSAGE_NOT_READABLE.getErrCode())
                .url(request.getRequestURL().toString())
                .status(NOT_ACCEPTABLE.value())
                .reqMethod(request.getMethod())
                .build();

        log.info("HttpMessageNotReadableException :: request.getMethod(): " + request.getMethod());
        return new ResponseEntity<>(error, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(JsonParseException.class)
    public ResponseEntity<Error> handleJsonParseException(HttpServletRequest request,
                                                          JsonParseException ex,
                                                          Locale locale) {
        ex.printStackTrace(); // TODO: Should be kept only for development
        Error error = Error.builder()
                .message(JSON_PARSE_ERROR.getErrMsgKey())
                .errorCode(JSON_PARSE_ERROR.getErrCode())
                .url(request.getRequestURL().toString())
                .status(NOT_ACCEPTABLE.value())
                .reqMethod(request.getMethod())
                .build();

        log.info("JsonParseException :: request.getMethod(): " + request.getMethod());
        return new ResponseEntity<>(error, INTERNAL_SERVER_ERROR);
    }
}
