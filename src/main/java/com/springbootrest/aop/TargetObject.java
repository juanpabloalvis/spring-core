package com.springbootrest.aop;

import org.springframework.stereotype.Service;

@Service
public class TargetObject {

//    private static final Logger log = LoggerFactory.getLogger(TargetObject.class);


    @MyAnnotation
    public void hello(String message) {
        System.out.println(String.format("el mensaje es: {}", message));
    }

    @MyAnnotation
    public static void helloStatic(String message) {
        System.out.println(String.format("static, el mensaje es: {%s}", message));
    }

    @MyAnnotation
    public static Object helloStaticWithReturn(String message) {
        System.out.println(String.format("static with return el mensaje es: {}", message));
        return message;
    }

    public void foo(String message) {
        System.out.println(String.format("el mensaje es: {}", message));
    }

}
