package com.springbootrest.aop;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Modifier;

@Component
@Aspect
@Order(1)
public class MyAspectTwo {
    private static final Logger log = LoggerFactory.getLogger(MyAspectTwo.class);

    /**
     * Este aspecto se ejecuta después del MyAspect.java que tiene el orden de 0
     */
//    @Before("MyPoincuts.targetObjectMethods()")
    @Before("@annotation(MyAnnotation)")
    public void before(JoinPoint joinPoint) {

        // Incluso el joinpoint pude acceder a las anotaciones del objeto.
        Signature signature = joinPoint.getSignature();
        log.info("Aspect 1, Modifiers: {}", signature.getModifiers());
        log.info("Aspect 1, Name: {}", signature.getName());
        log.info("Aspect 1, Return type: {}", signature.getDeclaringTypeName());
        log.info("Aspect 1, Is public: {}", Modifier.isPublic(joinPoint.getSignature().getModifiers()));
        // Esto es lo que más se usa, por ejemplo para validar parámetros
        log.info("Aspect 1, Args: {}", joinPoint.getArgs());

        log.info("Aspect 1, Before advice");
    }

    public void afterReturning() {
        log.info("After returning advice");
    }

    public void afterThrowing() {
        log.info("After trhowing advice");
    }

    public void afterFinally() {
        log.info("After finaly advice");
    }

    public void around() {
        log.info("Around advice");
    }
}
