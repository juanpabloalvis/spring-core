package com.springbootrest.aop;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(0)
//@Slf4j
public class MyAspect {

//
//    public void afterReturning() {
//        log.info("After returning advice");
//    }

    /**
     * este enunciado corresponde al pointcut que quiero intervernir.
     * Cualquier método de la clase TargetObject, el '*' corresponde al valor de retorno,
     * y el '*' es cualquier método de la clase, '(..) cualquier parámetro'
     * Para todos lo métodos:
     * execution(* com.springbootrest.aop.TargetObject.*(..))
     * Para el método hello
     * execution(* com.springbootrest.aop.TargetObject.hello(..))
     */
    //    @Before("execution(* com.springbootrest.aop.TargetObject.*(..))")
//    @Before("MyPoincuts.targetObjectMethods()")
//    @Around("@annotation(MyAnnotation) || execution(public * @MyAnnotation *.*(..)))")
//    @Around("@annotation(MyAnnotation)")
    @Before("execution(* com.springbootrest.aop.TargetObject.*(..))")
    public Object before(JoinPoint joinPoint) throws Throwable {

        System.err.println("**********************************************");
//        Object proceed = joinPoint.proceed();

        // Incluso el joinpoint pude acceder a las anotaciones del objeto.
        Signature signature = joinPoint.getSignature();
//        log.info("Aspect 0, Modifiers: {}", signature.getModifiers());
//        log.info("Aspect 0, Name: {}", signature.getName());
//        log.info("Aspect 0, Return type: {}", signature.getDeclaringTypeName());
//        log.info("Aspect 0, Is public: {}", Modifier.isPublic(joinPoint.getSignature().getModifiers()));
//        // Esto es lo que más se usa, por ejemplo para validar parámetros
//        log.info("Aspect 0, Args: {}", joinPoint.getArgs());
//
//        log.info("Aspect 0, Before advice");
        return null;
    }
//
//    public void afterThrowing() {
//        log.info("After trhowing advice");
//    }
//
//    public void afterFinally() {
//        log.info("After finaly advice");
//    }
//
//    public void around() {
//        log.info("Around advice");
//    }

}

