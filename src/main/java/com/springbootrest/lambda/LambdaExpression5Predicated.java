package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.LongPredicate;
import java.util.function.Predicate;

@Slf4j
public class LambdaExpression5Predicated {


    public static void main(String[] args) {
        Instructors instructors = new Instructors();

        // Predicate is single argument function and returns true of false
        Predicate<Integer> isGraterThanTen = x -> x > 10;

        log.info("[{}]", isGraterThanTen.test(324));

        // check if is even(par) and grater than
        Predicate<Integer> isEven = x -> x % 2 == 0;
        log.info("[{}]", isGraterThanTen.and(isEven).test(34));
        log.info("[{}]", isGraterThanTen.and(isEven).test(31));
        log.info("[{}]", isGraterThanTen.and(isEven).test(2));
        // check if is even(par) or grater than
        log.info("[{}]", isGraterThanTen.or(isEven).test(2));
        log.info("[{}]", isGraterThanTen.or(isEven).test(6));
        // check if is grater than ten and odd
        log.info("[{}]", isGraterThanTen.and(isEven.negate()).test(17));
        log.info("[{}]", isGraterThanTen.and(isEven.negate()).test(13));

        log.info("--------------print only if is online and more than ten years----------");

//        Predicate<Instructor> isOnline = instructor -> instructor.isOnlineCourses; is equal to:
        Predicate<Instructor> isOnline = Instructor::isOnlineCourses;

        Predicate<Instructor> yearsOfExperienceMoreThanTen = instructor -> instructor.getYearsOfExperience() > 10;
        instructors.getAll().forEach(instructor -> {
            if (isOnline.and(yearsOfExperienceMoreThanTen).test(instructor)) {
                log.info("[{}]", instructor);
            }
        });

        log.info("--------- Using predefined predicate with Integer----------");
        IntPredicate intPredicate = i -> i > 200;
        log.info("[{}]", intPredicate.negate().test(23));
        log.info("[{}]", intPredicate.negate().test(235));

        log.info("--------- Using predefined predicate with Long----------");
        LongPredicate longPredicate = l -> l > 200L;
        log.info("[{}]", longPredicate.test(23L));
        log.info("[{}]", longPredicate.test(235L));

        log.info("--------- Using predefined predicate with Double----------");
        DoublePredicate doublePredicate = l -> l > 100.5;
        log.info("[{}]", doublePredicate.test(100.8));
        log.info("[{}]", doublePredicate.test(100.4));


    }

    private LambdaExpression5Predicated() {
    }


}
