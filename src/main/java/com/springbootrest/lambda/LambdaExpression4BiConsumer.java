package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.BiConsumer;

@Slf4j
public class LambdaExpression4BiConsumer {


    public static void main(String[] args) {
        Instructors instructors = new Instructors();

        // Consumer takes two arguments and return/produce a result
        // in this case, takes an argument 'x' 'y' and perform some operation
        BiConsumer<String, Integer> biConsumer = (x, y) -> log.info(x.length() + " the value of x: " + x);

        BiConsumer<Integer, Integer> biConsumer2 = (x, y) -> log.info(" The sum of two numbers is: " + (x + y));

        biConsumer.accept("Foo", 5);
        biConsumer2.accept(5, 5);

        List<Instructor> instructorsList = instructors.getAll();

        // print age and gender
        log.info("--------------print out age and gender----------");
        BiConsumer<String, String> biConsumer3 = (name, gender) -> log.info("Name: " + name + ", Gender: " + gender);

        instructorsList.forEach(instructor
                -> biConsumer3.accept(instructor.getName(), instructor.getName()));

        // print out name and list of courses
        log.info("--------------print out name and list of courses---------------");
        BiConsumer<String, List<String>> biConsumer4 = (name, courses) -> log.info("Name: " + name + ", Courses: " + courses);
        instructorsList.forEach(instructor
                -> biConsumer4.accept(instructor.getName(), instructor.getCourses()));


        log.info("--------------print out name, gender who teach online---------------");
        instructorsList.forEach(instructor -> {
            if (instructor.isOnlineCourses()) {
                biConsumer3.accept(instructor.getName(), instructor.getGender());
            }
        });


    }

    private LambdaExpression4BiConsumer() {}


}
