package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntToDoubleFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

@Slf4j
public class LambdaExpression7Function {


    public static final String TO_LOWER_CASE = "To lower case: ";

    public static void main(String[] args) {
        Instructors instructors = new Instructors();

        // define the parameter of the function and the return:
        log.info("--------------Print the function usage-----------");
        @SuppressWarnings("java:S4276")
        Function<Integer, Double> sqrt = Math::sqrt;
        log.info("Raiz cuadrada1: " + sqrt.apply(25));
        log.info("Raiz cuadrada2: " + sqrt.apply(16));
        log.info("Raiz cuadrada3: " + sqrt.apply(17));

        log.info("--------Print the function with predefined interface-------");
        IntToDoubleFunction sqrtFunctional = Math::sqrt;
        log.info("Raiz cuadrada1: " + sqrtFunctional.applyAsDouble(25));
        log.info("Raiz cuadrada2: " + sqrtFunctional.applyAsDouble(16));
        log.info("Raiz cuadrada3: " + sqrtFunctional.applyAsDouble(17));

        log.info("--------------Print the function usage with String-----------");
        @SuppressWarnings("java:S4276")
        Function<String, String> lowerCase = String::toLowerCase;
        log.info(TO_LOWER_CASE + lowerCase.apply("THIS_IS A TEST"));
        log.info(TO_LOWER_CASE + lowerCase.apply("THIS_IS ANOTHER TEST"));

        log.info("--------------SameAsBefore-----------");
        UnaryOperator<String> sameAsBefore = String::toUpperCase;
        log.info(TO_LOWER_CASE + sameAsBefore.apply("Lorem ipsum test"));
        log.info(TO_LOWER_CASE + sameAsBefore.apply("Another lorem Ipsum"));

        UnaryOperator<String> concatFunction = "Concat as function: "::concat;

        log.info("--------------chain and compose -----------");
        log.info(concatFunction
                .andThen(sameAsBefore)
                .apply("This is another chained function"));
        // fist apply the function that is inside compose 'sameAsBefore', then apply the 'concat' function
        log.info(concatFunction
                .compose(sameAsBefore)
                .apply("This is another chained function"));

        log.info("----Map of instructors with name and years of experience -----------");
        log.info("----Map of instructors if is online  -----------");
        log.info("----Function which will List<Instructors> and return a Map<String, Integer> -----");

        Predicate<Instructor> onlineInstructor = Instructor::isOnlineCourses;
        Function<List<Instructor>, Map<String, Integer>> mapFunction = instructorsList -> {
            Map<String, Integer> map = new HashMap<>();
            instructorsList.forEach(instructor -> {
                        if (onlineInstructor.test(instructor)) {
                            map.put(instructor.getName(), instructor.getYearsOfExperience());
                        }
                    }
            );
            return map;
        };

        log.info("Map of name/yof experience" + mapFunction.apply(instructors.getAll()));

        log.info("----BiFunction which will List<Instructors> and number, then return a Integer of match -----");

        BiFunction<List<Instructor>, Integer, Integer> quantityOfInstructorsByYearsOfExperience = (instructorsList, yearsOfExperience) -> {
            int total = 0;
            for (Instructor instructor : instructorsList) {
                if (instructor.getYearsOfExperience() > yearsOfExperience) {
                    total++;
                }
            }
            return total;
        };

        log.info("Total of instructors with more than 5 year of experience: "
                + quantityOfInstructorsByYearsOfExperience.apply(instructors.getAll(), 5));
        log.info("Total of instructors with more than 3 year of experience: "
                + quantityOfInstructorsByYearsOfExperience.apply(instructors.getAll(), 3));
        log.info("Total of instructors with more than 15 year of experience: "
                + quantityOfInstructorsByYearsOfExperience.apply(instructors.getAll(), 15));

    }

    private LambdaExpression7Function() {
    }


}
