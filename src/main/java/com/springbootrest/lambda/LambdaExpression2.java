package com.springbootrest.lambda;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

@Slf4j
public class LambdaExpression2 {

    // examples of functional interface
    @FunctionalInterface
    public interface Runnable1 {
        public abstract void run();
    }

    @FunctionalInterface
    public interface Comparator<T> {
        int compare(T o1, T o2);
    }

    /**
     * Example
     *
     * @FunctionalInterface public interface Callable<V> {
     * V call() throws Exception;
     * }
     */

    protected static final int[] array = IntStream.rangeClosed(0, 5000).toArray();
    public static final int TOTAL = IntStream.rangeClosed(0, 5000).sum();

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        //        classic
        Callable<Integer> callable = () -> {
            int sum = 0;
            for (int i = 0; i < array.length / 2; i++) {
                sum += array[i];
            }
            return sum;
        };
        //        classic
        Callable<Integer> callable1 = () -> {
            int sum = 0;
            for (int i = array.length / 2; i < array.length; i++) {
                sum += array[i];
            }
            return sum;
        };

        // service to run threats
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        List<Callable<Integer>> taskList = Arrays.asList(callable, callable1);

        // the return is not righ now, it will be return on future.
        List<Future<Integer>> futureList = executorService.invokeAll(taskList);

        int k = 0;
        int sum = 0;
        for (Future<Integer> future : futureList) {
            sum = sum + future.get();
            log.info("Sum of: " + ++k + " is " + future.get());
        }

        log.info("Sum from the callable is " + sum);
        log.info("Correct sum of IntStream is: " + TOTAL);
        executorService.shutdown();
    }


}
