package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Predicate;

@Slf4j
public class LambdaExpression8BiFunction {


    public static void main(String[] args) {
        Instructors instructors = new Instructors();


        log.info("Bi Function: represent a function that takes two arguments as input and return a result");
        // Two inputs: 1) List<Instructors>, 2) Predicate<Instructor>, which will filter, 3) Return a map of <String, Integer>,
        // which is a name,years of experience
        Predicate<Instructor> onlineInstructor = Instructor::isOnlineCourses;

        BiFunction<List<Instructor>, Predicate<Instructor>, Map<String, Integer>> instructorNotOnline = (i, onlinePredicate) -> {
            Map<String, Integer> map = new HashMap<>();

            i.forEach(
                    instructor -> {
                        if (onlinePredicate.negate().test(instructor)) {
                            map.put(instructor.getName(), instructor.getYearsOfExperience());
                        }
                    }
            );
            return map;
        };

        Map<String, Integer> result = instructorNotOnline.apply(instructors.getAll(), onlineInstructor);
        log.info("Instructors by name and year of experience filter by predicate. " + result);
    }

    private LambdaExpression8BiFunction() {
    }


}
