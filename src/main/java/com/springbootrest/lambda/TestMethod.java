package com.springbootrest.lambda;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestMethod {

    private static final int NUMBER_FIVE = 5;
    private int length;

    public void myMethod(int inches) {
        if (length < NUMBER_FIVE) {
            int newSize = length + inches;
            length = newSize;
            log.info("Length: --> [{}]", length);
        }
    }

    public static void main(String[] args) {
        TestMethod testMethod = new TestMethod();
        testMethod.myMethod(1);
        testMethod.myMethod(2);
        testMethod.myMethod(3);
    }

    private TestMethod() {
    }
}
