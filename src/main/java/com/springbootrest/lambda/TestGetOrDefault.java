package com.springbootrest.lambda;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

@Slf4j
public class TestGetOrDefault {
    public static void main(String[] args) {
// Create a HashMap and add some values
        HashMap<String, Integer> map = new HashMap<>();
        map.put("a", 100);
        map.put("b", 200);
        map.put("c", 300);
        map.put("d", 400);
        // print map details
        log.info("HashMap: [{}]", map.toString());

        // provide key whose value has to be obtained
        // and default value for the key. Store the
        // return value in k
        int k = map.getOrDefault("b", 500);

        // print the value of k returned by
        // getOrDefault(Object key, V defaultValue) method
        log.info("Returned Value: [{}]", k);

        log.info("Get of default: [{}]", map.getOrDefault("7", 500));
    }

    private TestGetOrDefault() {
    }
}
