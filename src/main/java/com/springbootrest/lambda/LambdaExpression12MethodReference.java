package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

@Slf4j
public class LambdaExpression12MethodReference {


    public static void main(String[] args) {
        Instructors instructors = new Instructors();
        Instructor instructor = instructors.getAll().get(0);
        log.info("Function method reference");
        /**
         * Before refactor:
         *         Function<Integer, Double> squr0 = integer -> Math.sqrt(integer);
         */
        IntFunction<Double> sqrt = Math::sqrt;
        log.info("[{}]", sqrt.apply(25));

        /**
         * Before refactor:
         *          Function<String, String> toUpperCase = (str) -> str.toUpperCase();
         */
        Function<String, String> toUpperCase = String::toUpperCase;
        log.info("[{}]", toUpperCase.apply("Damm"));

        /**
         * Before refactor:
         *           Predicate<Instructor> isOnline = instructor -> instructor.isOnlineCourses();
         */
        Predicate<Instructor> isOnline = Instructor::isOnlineCourses;
        log.info("Is Online [{}]", isOnline.test(instructor));


        /**
         *  Expression to convert to method reference
         *         Predicate<Instructor> yearsOfExperienceMoreThanTen = instructor -> instructor.getYearsOfExperience() > 10;
         */
        log.info("Cuando hay logic no se puede convertir a method reference, " +
                "pero creando un método lo podremos hacer");
        Predicate<Instructor> isGraterThanTen = LambdaExpression12MethodReference::isGraterThanTen;

        log.info("Has more than ten years of experience [{}]", isGraterThanTen.test(instructor));
        instructors.getAll().forEach(instr -> {
            if (isGraterThanTen(instr)) {
                log.info("> Has more than ten years of experience [{}]", instr);
            }
        });

        /**
         * After create the FactoryInterface with the abstract method that return the same type, it is possible to
         * use the new as reference.
         */
        InstructorFactory instructorFactory = Instructor::new;
        Instructor instructor1 = instructorFactory.getInstructor("Juan", 14, "Developer", "Male", true, Arrays.asList("Spring", "Python", "SQL"));
        log.info("Instructor: " + instructor1);
    }

    public interface InstructorFactory {
        public abstract Instructor getInstructor(String name, int yearsOfExperience, String title, String gender, boolean onlineCourses, List<String> courses);

    }

    public static boolean isGraterThanTen(Instructor instructor) {
        return instructor.getYearsOfExperience() > 10;
    }

    private LambdaExpression12MethodReference() {
    }

}


