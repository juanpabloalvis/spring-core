package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

@Slf4j
public class LambdaExpression3Consumer {

    public static void main(String[] args) {
        // Consumer takes one argument and return and produce a result
        // in this case, takes an argument 'x' and perform some operation
        Consumer<String> c = x -> log.info(x.length() + " the value of x: " + x);
        // accept, start to performing the operation
        c.accept("Up in the air");


        // Consumer with block statement
        Consumer<Integer> digit = x -> {
            log.info(" x * 2: " + (x * 2));
            log.info(" x * x: " + (x * x));
        };
        digit.accept(25);

        // same as before, it is better use IntConsumer
        IntConsumer intConsumer = value -> {
            log.info(" x * x: " + (value * value));
            log.info(" x + x: " + (value + value));
        };

        intConsumer.accept(25);

        // Looping through all instructors and printing the values of instructor
        Instructors instructors = new Instructors();
        List<Instructor> all = instructors.getAll();
        Consumer<Instructor> c1 = s1 -> log.info("Obj: " + s1);
        //  foreach with consumer parameter will call accept method for foreach.
        all.forEach(c1);

        // Looping through all instructors and printing only name
        log.info("-----------------------------------------------");
        Consumer<Instructor> cName = s1 -> log.info("Obj: " + s1.getName());
        all.forEach(cName);

        // Looping through all instructors and printing name and courses
        log.info("-------------------WOW------------------------");
        Consumer<Instructor> cCourses = s1 -> log.info("[{}]", s1.getCourses());
        all.forEach(cName.andThen(cCourses));

        // Looping through all instructors and printing name if experience is > 10
        // here cName, is because we call the cName defined function.
        log.info("-------------------WOW 2------------------------");
        all.forEach(instructor -> {
            if (instructor.getYearsOfExperience() > 10) {
                cName.accept(instructor);
            }
        });

        // Looping through all instructors and printing name if experience is > 5 and teaches online
        // here cName, is because we call the cName defined function.
        log.info("-------------------WOW 2------------------------");
        all.forEach(instructor -> {
            if (instructor.getYearsOfExperience() > 5 && instructor.isOnlineCourses()) {
                c1.andThen(cName).accept(instructor);
            }
        });


    }


}
