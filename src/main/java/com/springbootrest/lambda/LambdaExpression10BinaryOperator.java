package com.springbootrest.lambda;

import lombok.extern.slf4j.Slf4j;

import java.util.Comparator;
import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.IntBinaryOperator;
import java.util.function.LongBinaryOperator;

@Slf4j
public class LambdaExpression10BinaryOperator {


    public static void main(String[] args) {

        log.info("BinaryOperator extends from BiFunction, It takes a two parameter and returns a single value. " +
                "Both parameters and return type must be the same type\n" +
                "Useful when implement math operations like sume, subtract, multiply and divide, etc.");

        BinaryOperator<Integer> sumWithBinaryOperator = (x, y) -> x + y;
        log.info("Sum with binary operator: " + sumWithBinaryOperator.apply(5, 4));

        /**
         * Before refactor:
         *         Comparator<Integer> comparator = (o1, o2) -> o1.compareTo(o2);
         */
        Comparator<Integer> comparator = Integer::compareTo;
        BinaryOperator<Integer> maxBi = BinaryOperator.maxBy(comparator);
        log.info("BinaryOperator with Comparator getting max: " + maxBi.apply(5, 45));

        BinaryOperator<Integer> minBy = BinaryOperator.minBy(comparator);
        log.info("BinaryOperator with Comparator getting min: " + minBy.apply(5, 45));

        /**
         * Before refactor:
         *         IntBinaryOperator sumWithIntBinaryOperator = (x, y) -> x + y;
         */
        IntBinaryOperator sumWithIntBinaryOperator = Integer::sum;
        log.info("Sum with IntBinaryOperator: " + sumWithIntBinaryOperator.applyAsInt(5, 4));

        DoubleBinaryOperator multiplyWithDoubleBinaryOperator = (x, y) -> x * y;
        log.info("Multiply with DoubleBinaryOperator: " + multiplyWithDoubleBinaryOperator.applyAsDouble(5.5, 4.5));

        LongBinaryOperator multiplyWithLongBinaryOperator = (x, y) -> x / y;
        log.info("Multiply with LongBinaryOperator: " + multiplyWithLongBinaryOperator.applyAsLong(500000000000005L, 40000000000000005L));

    }

    private LambdaExpression10BinaryOperator() {
    }


}
