package com.springbootrest.lambda.bank;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;

@Slf4j
public class BankTransfer {
    public static void main(String[] args) {
        AccountFactory accountFactory = BankAccount::new;
        BankAccount studentBankAccount = accountFactory.getBankAccount(1, 50000, "Student A");
        BankAccount universityBankAccount = accountFactory.getBankAccount(2, 100000, "University");

        BiPredicate<Double, Double> p1 = (balance, amount) -> balance > amount;
        BiConsumer<String, Double> printer = (x, y) -> log.info(" x + y: [{}]", (x + y));
        BiConsumer<BankAccount, BankAccount> printer2 = (student, university) -> {
            log.info("Ending balance of student account: [{}]. University bank account: [{}]. ", studentBankAccount.getBalance(), universityBankAccount.getBalance());
        };
        ExecutorService service = Executors.newFixedThreadPool(10);

        Thread t1 = new Thread(() -> {
            log.info(" says :: Executing Service [{}]", Thread.currentThread().getName());
            try {
                double amount = 1000;
                if (!p1.test(studentBankAccount.getBalance(), amount)) {
                    printer.accept(Thread.currentThread().getName() + " says :: balance insufficient, ", amount);
                    return;
                }
                while (!studentBankAccount.transferTo(universityBankAccount, amount)) {
                    TimeUnit.MILLISECONDS.sleep(100);
                    continue;
                }
            } catch (InterruptedException e) {
                log.info("Interrupted!", e);
                e.printStackTrace();
            }
            printer.accept(Thread.currentThread().getName() + " says :: Transference is successful: ", universityBankAccount.getBalance());

        });

        for (int i = 0; i < 20; i++) {
            service.submit(t1);
        }
        service.shutdown();

        try {
            while (!service.awaitTermination(24L, TimeUnit.HOURS)) {
                log.info("No yet. Still waiting for termination.");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        printer2.accept(studentBankAccount, universityBankAccount);


    }

    private BankTransfer() {
    }
}
