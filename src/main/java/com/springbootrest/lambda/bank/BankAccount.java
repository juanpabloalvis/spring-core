package com.springbootrest.lambda.bank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiFunction;

@Data
@Builder
@AllArgsConstructor
@Slf4j
public class BankAccount {
    private Integer id;
    private Double balance;
    private String accountName;
    final Lock lock = new ReentrantLock();
    static final BiFunction<Double, Double, Double> subtractFunction = (aDouble, aDouble2) -> aDouble - aDouble2;
    static final BiFunction<Double, Double, Double> addFunction = Double::sum;

    public boolean withdraw(double amount) throws InterruptedException {
        if (this.lock.tryLock()) {
            Thread.sleep(100);
            balance = subtractFunction.apply(balance, amount);
            this.lock.unlock();
            return true;
        }
        return false;
    }

    public boolean deposit(double amount) throws InterruptedException {
        if (this.lock.tryLock()) {
            Thread.sleep(100);
            balance = addFunction.apply(balance, amount);
            this.lock.unlock();
            return true;
        }
        return false;
    }

    public boolean transferTo(BankAccount to, double amount) throws InterruptedException {
        if (withdraw(amount)) {
            log.info("Withdrawing amount: [{}] from: [{}] ", amount, to.accountName);
            if (to.deposit(amount)) {
                log.info("Depositing amount: [{}] to: [{}] ", amount, to.accountName);
                return true;
            }
            log.info("Failed to acquire both lockers");
            while (deposit(amount)){}


        }
        return false;
    }
}
