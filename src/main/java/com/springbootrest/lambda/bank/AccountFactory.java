package com.springbootrest.lambda.bank;

public interface AccountFactory {
    BankAccount getBankAccount(Integer id, double balance, String accountName);
}
