package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.function.DoubleUnaryOperator;
import java.util.function.IntUnaryOperator;
import java.util.function.LongUnaryOperator;
import java.util.function.UnaryOperator;

@Slf4j
public class LambdaExpression9UnaryOperator {


    public static void main(String[] args) {
        Instructors instructors = new Instructors();


        log.info("UnaryOperator Extends Function, It takes a single parameter and returns a parameter of the same type");
        //
        UnaryOperator<Instructor> upperCaseInstructorNameAndTitle = instructor -> {
            instructor.setName(instructor.getName().toUpperCase());
            instructor.setTitle(instructor.getTitle().toUpperCase());
            return instructor;
        };

        var instructor = upperCaseInstructorNameAndTitle.apply(instructors.getAll().get(0));
        log.info("Instructors by name and year of experience filter by predicate. " + instructor);


/**
 * Similar to:
 *          UnaryOperator<Integer> multiplyByTen = x -> x * 10;
 *          Function<Integer, Integer> multiplyByTen = x -> x * 10;
 *
 */
        IntUnaryOperator multiplyByTen = x -> x * 10;
        log.info("Unary, multiply by 10: . " + multiplyByTen.applyAsInt(5));

        DoubleUnaryOperator doubleMultiplyByTen = x -> x * 10;
        log.info("Unary, multiply by 10 with double: . " + doubleMultiplyByTen.applyAsDouble(5));

        LongUnaryOperator longMultiplyByTen = x -> x * 10;
        log.info("Unary, multiply by 10 with long: . " + longMultiplyByTen.applyAsLong(5000000000000000001L));


    }

    private LambdaExpression9UnaryOperator() {
    }


}
