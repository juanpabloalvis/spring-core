package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.function.IntSupplier;

@Slf4j
public class LambdaExpression11Supplier {


    public static void main(String[] args) {
        Instructors instructors = new Instructors();
        Instructor instructor = instructors.getAll().get(0);
        log.info("Is a functional interface that supplies a value of some type\n" +
                "NOTE: It can be thought of a factory interface");

/**
 *Before refactor:
 *          Supplier<Integer> supplierInt = () -> (int) (Math.random() * 1000);
 */
        IntSupplier supplierInt = () -> (int) (Math.random() * 1000);
        log.info("Suplier interface getInt" + supplierInt.getAsInt());
        log.info("Suplier interface getInt" + supplierInt.getAsInt());
        log.info("Suplier interface getInt" + supplierInt.getAsInt());
//        Supplier<Instructor> supplier = () -> Math.random() *1000;


    }

    private LambdaExpression11Supplier() {
    }


}
