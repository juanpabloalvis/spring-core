package com.springbootrest.lambda;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class LambdaExpression1 {

    // examples of functional interface
    @FunctionalInterface
    public interface Runnable {
        public abstract void run();
    }

    public interface HelloWorldOldInterface {
        public String sayHelloWorld();
    }

    @FunctionalInterface
    public interface HelloWorldNewInterface {
        public String sayHelloWorld();

        // if is default, it has implementation
        default String shePlaysTennis(String she) {
            return she.toUpperCase();
        }
    }

    @FunctionalInterface
    public interface OperateNumbers {
        public abstract Integer operate(Integer n1, Integer n2);
    }

    public static class HelloWorldOldInterfaceImpl implements HelloWorldOldInterface {
        @Override
        public String sayHelloWorld() {
            return "Hello World";
        }
    }

    public static void main(String[] args) {
        HelloWorldOldInterfaceImpl helloWorldInterface = new HelloWorldOldInterfaceImpl();
        log.info(helloWorldInterface.sayHelloWorld());

        // I can implement as lambda when I have defined as @FunctionalInterface
        HelloWorldNewInterface helloWorldNewInterface = () -> "Hello new World"; // or { return "Hello new World";};
        log.info(helloWorldNewInterface.sayHelloWorld());
        log.info(helloWorldNewInterface.shePlaysTennis("Hi there"));

        // Implement interface
        OperateNumbers subNumbers = (n1, n2) -> n1 - n2;
        OperateNumbers sumNumbers = Integer::sum;
        OperateNumbers multiplyNumbers = (n1, n2) -> n1 * n2;

        List<OperateNumbers> operateNumbers = Arrays.asList(subNumbers, sumNumbers, multiplyNumbers);

        // calling individual
        log.info("[{}]", subNumbers.operate(50, 10));
        log.info("[{}]", sumNumbers.operate(10, 20));
        log.info("[{}]", multiplyNumbers.operate(10, 20));

        // same as before
        for (OperateNumbers operation : operateNumbers) {
            log.info("" + operation.operate(50, 10));
        }

        // same as before with foreach
        operateNumbers.forEach(o -> {
            log.info("" + o.operate(50, 10));
        });
    }

}
