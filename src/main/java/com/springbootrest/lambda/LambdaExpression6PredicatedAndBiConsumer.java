package com.springbootrest.lambda;

import com.springbootrest.lambda.dto.Instructor;
import com.springbootrest.lambda.dto.Instructors;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

@Slf4j
public class LambdaExpression6PredicatedAndBiConsumer {


    public static void main(String[] args) {
        Instructors instructors = new Instructors();

        Predicate<Instructor> isOnline = Instructor::isOnlineCourses;
        Predicate<Instructor> yearsOfExperienceMoreThanTen = instructor -> instructor.getYearsOfExperience() > 10;
        BiConsumer<String, List<String>> biConsumer = (name, courses) -> log.info("Name: " + name + ", Courses: " + courses);

        log.info("-------------- Print biConsumer ----------------");

        instructors.getAll().forEach(instructor -> {
            if (isOnline.and(yearsOfExperienceMoreThanTen).test(instructor)) {
                biConsumer.accept(instructor.getName(), instructor.getCourses());
            }
        });

        log.info("-------------- Print biPredicate and biConsumer ----------------");
        BiPredicate<Boolean, Integer> biPredicate = (online, experience) -> online && experience > 5;
        instructors.getAll().forEach(instructor -> {
            if (biPredicate.test(instructor.isOnlineCourses(), instructor.getYearsOfExperience())) {
                biConsumer.accept(instructor.getName(), instructor.getCourses());
            }
        });

    }

    private LambdaExpression6PredicatedAndBiConsumer() {
    }


}
