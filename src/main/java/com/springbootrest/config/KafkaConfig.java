package com.springbootrest.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConfig {


    /**
     * configurar un Consumer
     */
    private Map<String, Object> consumerProps() {
        Map<String, Object> props = new HashMap<>();
        // Lista de brokers de kafka en el cluster.
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Consumer group que consumirá los mensajes
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "jp-group");
        // Determina si se hará commit al offset de forma periódica
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        // Determina la frecuencia en milisegundos en
        // la que se hará commit a los offsets, solo es necesaria si
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "100");
        // Timeout utilizado para determinar errores en los clientes.
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "15000");
        //
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "15000");
        // Se requiere porque los msg tienen una key-value->Integer-String
        // Clase a utilizar para deserializar la llave.
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
        // Clase a utilizar para deserializar el mensaj
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return props;
    }

    /**
     * configurar un Producer
     */
    private Map<String, Object> producerProps() {
        Map<String, Object> props = new HashMap<>();
        // Lista de brokers de kafka en el cluster.
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // Reintentos en caso de error
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        // El producer agrupará los registros en batches, mejorando el performance (está definido en bytes), cuando se llene, crea otro.
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        // Los batches se agruparan de acuerdo de un periodo de tiempo, está definido en milisegundos.
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        // Define el espacio de memoria que se asignará para colocar los mensajes que están pendientes por enviar.
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        // aqui van los serializadores
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);


        return props;
    }

    @Bean
    public KafkaTemplate<Integer, String> createTemplate() {
        Map<String, Object> senderProps = producerProps();
        ProducerFactory<Integer, String> pf = new DefaultKafkaProducerFactory<Integer, String>(senderProps);
        return new KafkaTemplate<>(pf);
    }


    /**
     * configurar un producer
     * <p>
     * Es el que permite recibir los mensajes y permite hacer algo con ellos.
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<Integer, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    /**
     * Se utiliza para crear el listener, es el que permite recibir los mensaje
     *
     * @return un consumer factory de kafka
     */
    @Bean
    public ConsumerFactory<Integer, String> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerProps());
    }


}
