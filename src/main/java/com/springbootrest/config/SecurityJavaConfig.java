package com.springbootrest.config;

import com.springbootrest.services.UserDetailCustomService;
import lombok.RequiredArgsConstructor;
import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityJavaConfig {

    private final PasswordEncoder passwordEncoder;
    private final UserDetailCustomService userDetailCustomService;


    // Comentamos porque utilizamos autenticación de la BD
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//
//        auth.inMemoryAuthentication()
//                .withUser("admin").password(encoder().encode("admin")).roles("ADMIN")
//                .and()
//                .withUser("user").password(encoder().encode("user")).roles("USER")
//        ;
//    }
    @Bean
//    @SuppressWarnings("unused")
    public AuthenticationManager authenticationManagerBean(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http.csrf().disable()// deshabilitamos csrf
                .authorizeRequests()
                .antMatchers("/users/authenticate").permitAll()
                .antMatchers("/users/auth").permitAll()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
//                .antMatchers("/users/**").hasRole("ADMIN") // Cual end point voy a proteger
                .antMatchers("/hello").authenticated()
                .antMatchers("/roles/**").authenticated() // Todos los recursos solo requieren autenticacion sin importar el role
                .and().httpBasic()
                .and().build();
//        http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//                .authorizeRequests().antMatchers("/users/authenticate").permitAll().anyRequest().authenticated();

//        http.apply(new JwtTokenConfigurer(tokenProvider));
    }

    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        // Se define la clase que recupera los usuarios y el algoritmo para procesar las passwords
        auth.userDetailsService(userDetailCustomService).passwordEncoder(passwordEncoder);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

    // to allow se h2 console in browser
    @Bean
    org.h2.tools.Server h2Server() {
        Server server = new Server();
        try {
            server.runTool("-tcp");
//            server.runTool("-tcpAllowOthers");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return server;

    }

   }

