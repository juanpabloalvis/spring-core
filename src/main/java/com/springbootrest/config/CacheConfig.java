package com.springbootrest.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfig {


    //Configuración de caché local.
    @Bean
    public CacheManager getCache() {
        return new ConcurrentMapCacheManager("users", "role", "address");
    }

    //Configuración de caché en un servidor de caché (REDIS).
//    @Bean(destroyMethod = "shutdown")
//    public RedissonClient redisson() {
//        Config config = new Config();
//
//        // podrían ser multiples nodos
//        config.useSingleServer().setAddress("redis://127.0.0.1:6379"); //
//        return Redisson.create(config);
//    }

//    @Bean
//    public CacheManager cacheManager(RedissonClient redissonClient) {
//        Map<String, CacheConfig> config = new HashMap<>();
//        config.put("testMap", new CacheConfig());
//        return new RedissonSpringCacheManager(redissonClient);
//    }


}
