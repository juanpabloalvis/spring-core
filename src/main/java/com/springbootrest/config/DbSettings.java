package com.springbootrest.config;


import com.github.javafaker.Faker;
import com.springbootrest.models.Role;
import com.springbootrest.models.User;
import com.springbootrest.models.UserInRole;
import com.springbootrest.repositories.RoleRepository;
import com.springbootrest.repositories.UserInRoleRepository;
import com.springbootrest.repositories.UserRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Random;

@RequiredArgsConstructor
@Component
public class DbSettings implements ApplicationRunner {

    private final Faker faker;


    private final UserRepository repository;


    private final RoleRepository roleRepository;

    private final UserInRoleRepository userInRoleRepository;

    private static final Logger log = LoggerFactory.getLogger(DbSettings.class);

//    @Autowired
//    public DbSettings(Faker faker) {
//        this.faker = faker;
//    }


    @Override
    public void run(ApplicationArguments args) {
        Role[] roles = {
                new Role("ADMIN", "Admin full permision"),
                new Role("USER", "Simple User"),
                new Role("SUPPORT", "The support guy")};

        roleRepository.saveAll(Arrays.asList(roles));

        addDefaultAdminUser(roles);

        for (int i = 0; i < 50; i++) {
            User user = new User();
            user.setNickName(faker.funnyName().name());
            user.setUsername(faker.name().username());
            user.setPassword(faker.dragonBall().character());
            User save = repository.save(user);
            UserInRole userInRole = new UserInRole(save, roles[new Random().nextInt(3)]);
            log.info("Usuario nombre [{}], password [{}], role [{}]", save.getUsername(), save.getPassword(), userInRole.getRole().getName());
            userInRoleRepository.save(userInRole);
        }
    }

    private void addDefaultAdminUser(Role[] roles) {
        User useradmin = new User();
        useradmin.setUsername("admin1");
        useradmin.setNickName("admin1");
        useradmin.setPassword("admin1");
        User save = repository.save(useradmin);
        UserInRole userInRole = new UserInRole(save, roles[0]);
        userInRoleRepository.save(userInRole);
    }
}
