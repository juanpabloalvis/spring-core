package com.springbootrest.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@Builder
@EqualsAndHashCode
public class Cuenta {
    private String persona;
    private BigDecimal saldo;



}
