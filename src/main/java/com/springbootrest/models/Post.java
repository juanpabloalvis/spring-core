package com.springbootrest.models;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.time.LocalDateTime;

@Builder
@Data
@Document("post")
public class Post {
    @Id
    private String id;
    private String title;
    private String description;
    private String content;
    private String author;
    private LocalDateTime creationDate;
}
