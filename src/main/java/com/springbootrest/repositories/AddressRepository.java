package com.springbootrest.repositories;

import com.springbootrest.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
    @Query("SELECT a FROM Address a Where a.profile.user.id = ?1 and a.profile.id = ?2")
    List<Address> findByProfileId(Integer userId, Integer profileId);
}
