package com.springbootrest.lifecycle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * Crea un evento por cada bean que es creado, esto nos permitirá ver que beans están siendo construidos
 * se puede filtar por tipo de
 */
@Component
public class JPBeanPostProcessor implements BeanPostProcessor {
    private static final Logger log = LoggerFactory.getLogger(JPBeanPostProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        // aquí puedo agregar cierta lógica que se ejecute antes de la inicialización de cierto bean
        if (bean instanceof LifeCycleBean) {
            log.info("BEFORE initialization LIFECYCLEBEAN {}", beanName);
        }

        if (bean instanceof LifeCycleBean) {
            log.info("BEFORE initialization LIFECYCLEBEAN {}", beanName);
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        // aquí puedo agregar cierta lógica que se ejecute antes de la inicialización de cierto bean
        if (bean instanceof LifeCycleBean) {
            log.info("AFTER initialization LIFECYCLEBEAN {}", beanName);
        }
        return bean;
    }
}
