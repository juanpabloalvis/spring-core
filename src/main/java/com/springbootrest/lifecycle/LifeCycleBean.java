package com.springbootrest.lifecycle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
//@Scope("prototype") -- Recordemos que no se ejecutará el método pre-destroy en este scope
//@Lazy // se inizializa solo cuando se llama, y eager siempre se inizializa así no se haga nada con ese bean.
//@Lazy(value = false) //, y eager siempre se inizializa así no se haga nada con ese bean.
public class LifeCycleBean implements BeanNameAware, InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(LifeCycleBean.class);

    /**
     * Se ejecutará durante la construcción del bean -Bean name aware-
     */
    @Override
    public void setBeanName(String name) {
        log.info("Bean name aware {}", name);

    }

    /**
     * Se ejecutará después de la inyección de dependencias.
     */
    @PostConstruct
    public void init() {
        log.info("Post constructor");
    }

    /**
     * Se ejecutará antes de que el bean sea destruido y no se ejecutarán para beans Prototypes.
     * Y terminan sólo si la VM termina de forma normal.
     */
    @PreDestroy
    public void destroyBean() {
        log.info("Pre destroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("After properties set method");

    }

    @Override
    public void destroy() throws Exception {
        log.info("Destroy  method");
    }
}
