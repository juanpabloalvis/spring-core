package com.springbootrest.processors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldBean {

    private static final Logger log = LoggerFactory.getLogger(HelloWorldBean.class);

    private String message;

    public void setMessage(String message){
        this.message  = message;
    }
    public void getMessage(){
       log.info("Your Message : [{}]", message);
    }
    public void init(){
       log.info("Bean is going through init.");
    }
    public void destroy(){
       log.info("Bean will destroy now.");
    }
}
