package com.springbootrest.processors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class InitHelloWorld implements BeanPostProcessor {

    private static final Logger log = LoggerFactory.getLogger(InitHelloWorld.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof HelloWorldBean) {
            log.info("BeforeInitialization HelloWorldBean: [{}]", beanName);
        }

        return bean;  // you can return any other object as well
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof HelloWorldBean) {
            log.info("AfterInitialization HelloWorldBean: [{}]", beanName);
        }
        return bean;  // you can return any other object as well
    }
}