package com.springbootrest.scopes;

import org.springframework.stereotype.Service;

@Service
//@Scope("singleton") Por defecto los beans en spring son singleton
public class EjemploScopeDefaultService {
}
