package com.springbootrest;

import com.springbootrest.aop.TargetObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@Slf4j
public class SpringCoreApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(SpringCoreApplication.class, args);
        TargetObject bean = context.getBean(TargetObject.class);
        bean.hello("Hola que tal");
        bean.foo("Este metodo no tiene asoiado en el aspecto");
    }

}
