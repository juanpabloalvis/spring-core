package com.springbootrest.services;

import com.springbootrest.models.Post;
import com.springbootrest.repositories.PostRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
@Slf4j
public class PostsService {

    private final PostRepository postRepository;


    public Flux<Post> getAll() {
        Flux<Post> all = postRepository.findAll();
        log.info("Found [{}] rows", all.count().log());
        return all;
    }

    public Mono<Post> createPost(Post post) {
        if (post.getId() == null) {
            Mono<Post> save = postRepository.save(post);
            return save;
        }
        log.info("Found existent post id [{}]", post.getId());

        return Mono.error(new IllegalArgumentException(String.format("The provided post already exist with id [%s]", post.getId())));

    }

    public Mono<Post> findById(String id) {
        Mono<Post> findById = postRepository.findById(id);
        return findById;
    }

    public Mono<Void> deleteById(String id) {
        Mono<Post> findById = postRepository.findById(id);
        return findById
                .switchIfEmpty(Mono.error(new IllegalArgumentException(String.format("The provided post does not exist with id [%s]", id))))
                .flatMap(postRepository::delete);
    }

    public ResponseEntity<String> updatePost(Post post) {
        return null;
    }
}
