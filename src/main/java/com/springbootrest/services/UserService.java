package com.springbootrest.services;

import com.springbootrest.models.User;
import com.springbootrest.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class UserService {
    private final UserRepository repository;

    public Page<User> getUsers(int page, int size) {
        Page<User> all = repository.findAll(PageRequest.of(page, size));
        return all;
    }

    public Page<String> getUsernames(int page, int size) {
//        List<User> all = repository.findUsername(ListRequest.of(page, size));
        Page<String> all = repository.findAllUserNames(PageRequest.of(page, size));
        return all;
    }

    public User getUserById(Integer userId) {
        return repository.findById(userId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("User with id %d does not exists", userId)));
    }

    @Cacheable(value = "users")
    public User getUserByName(String username) {

        log.info("Busqueda de Usuarios by username");
        return repository.findByUsername(username).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("User with username %s does not exists", username)));
    }

    public User getUserByNameAndPassword(String username, String password) {
        return repository.findByUsernameAndPassword(username, password).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("User with username %s and password %s does not exists", username, password)));
    }

    @CacheEvict(value = "users", allEntries = true)
    public User createUser(User role) {
        return repository.save(role);
    }

    @CacheEvict(value = "users", allEntries = true)
    public User updateUser(User userToUpdate, Integer userId) {
        Optional<User> result = repository.findById(userId);
        if (result.isPresent()) {
            return repository.save(userToUpdate);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User id %d does not exists", userId));
    }

    @CacheEvict(value = "users", allEntries = true)
    public void deleteUser(Integer userId) {
        Optional<User> result = repository.findById(userId);
        if (result.isPresent()) {
            repository.delete(result.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User id %d does not exists", userId));
        }
    }


}
