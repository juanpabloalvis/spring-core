package com.springbootrest.services;

import com.springbootrest.models.User;
import com.springbootrest.models.UserInRole;
import com.springbootrest.repositories.UserInRoleRepository;
import com.springbootrest.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserDetailCustomService implements UserDetailsService {

    private final UserRepository userRepository;

    private final UserInRoleRepository userInRoleRepository;

    private final PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Nosotros buscamos al usuario y Spring es el encargado de realizar la autenticación

        Optional<User> optional = userRepository.findByUsername(username);

        if (optional.isPresent()) {
            User user = optional.get();
            // Lista de roles por usuarios
            List<UserInRole> userInRoles = userInRoleRepository.findByUser(user);
            String[] roles = userInRoles.stream().map(r -> r.getRole().getName()).toArray(String[]::new);
            return
                    org.springframework.security.core.userdetails.User
                            .withUsername(user.getUsername())
                            .password(passwordEncoder.encode(user.getPassword())).roles(roles).build();
        } else {
            throw new UsernameNotFoundException(String.format("Username %s not found", username));
        }
    }

}

