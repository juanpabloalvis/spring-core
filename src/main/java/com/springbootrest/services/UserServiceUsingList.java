package com.springbootrest.services;

import com.github.javafaker.Faker;
import com.springbootrest.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserServiceUsingList {

    private final Faker faker;

    private Set<User> users = new HashSet<>();

    @PostConstruct
    public void init() {

        // Creamos cien usuarios
        for (int i = 0; i < 100; i++) {
            users.add(new User(i, faker.funnyName().name(), faker.name().username(), faker.dragonBall().character()));
        }
    }

    public Set<User> getUsers(@RequestParam(value = "startWith", required = false) String startWith) {
        if (startWith != null) {
            return users.stream().filter(user -> user.getUsername().startsWith(startWith)).collect(Collectors.toSet());
        }
        return users;
    }

    public User getUserByName(String userName) {
        return users.stream().filter(
                user -> user.getUsername().equals(userName)).findAny()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("User %s not found", userName)));
    }

    public User createUser(User user) {
        if (users.stream().anyMatch(u -> u.getUsername().equals(user.getUsername()))) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("User %s already exists", user.getUsername()));
        }
        users.add(user);
        return user;
    }

    public User updateUser(User user, String username) {
        User userToBeUpdate = getUserByName(username);
        userToBeUpdate.setNickName(user.getNickName());
        userToBeUpdate.setPassword(user.getPassword());
        return userToBeUpdate;
    }

    public void deleteUser(String username) {
        User userToBeDelete = getUserByName(username);
        users.remove(userToBeDelete);
    }
}
