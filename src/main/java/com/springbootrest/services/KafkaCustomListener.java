package com.springbootrest.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaCustomListener {
    @KafkaListener(topics = "jp-topic", groupId = "jp-group")
    public void listner(String message) {
        log.info("Message received [{}]", message);
    }
}
