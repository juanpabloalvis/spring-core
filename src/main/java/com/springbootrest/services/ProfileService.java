package com.springbootrest.services;

import com.springbootrest.models.Profile;
import com.springbootrest.models.User;
import com.springbootrest.repositories.ProfileRepository;
import com.springbootrest.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProfileService {

    private static final Logger log = LoggerFactory.getLogger(ProfileService.class);


    private final ProfileRepository profileRepository;
    private final UserRepository userRepository;

    public Profile createProfile(Integer userId, Profile profile) {
        Optional<User> result = userRepository.findById(userId);
        if (result.isPresent()) {
            profile.setUser(result.get());
            return profileRepository.save(profile);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User %s not found", userId));
    }


    public Profile getByUserIdAndProfileId(Integer userId, Integer profileId) {
        return profileRepository.findByUserIdAndId(userId, profileId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Profile not found for userId %d and profileId %d ", userId, profileId)));

    }

    public Profile updateProfile(Integer userId, Profile profile) {
        return null;
    }

    public void deleteProfile(Integer userId, Integer profileId) {
        Profile result = getByUserIdAndProfileId(userId, profileId);
//        if (result.isPresent()) {
//            repository.delete(result.get());
//        } else {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User id %d does not exists", userId));
//        }
    }
}
