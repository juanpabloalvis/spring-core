package com.springbootrest.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springbootrest.models.AuditDetails;
import com.springbootrest.models.Role;
import com.springbootrest.models.User;
import com.springbootrest.repositories.RoleRepository;
import com.springbootrest.repositories.UserInRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class RoleService {

    private final RoleRepository repository;

    private final UserInRoleRepository userInRoleRepository;

    private final KafkaTemplate<Integer, String> kafkaTemplate;

    private ObjectMapper mapper = new ObjectMapper();

    public List<User> getUsersByRole(String roleName) {
        return userInRoleRepository.findUsersByRole(roleName);
    }

    public List<Role> getRoles() {
        return repository.findAll();
    }


    public Role createRole(Role role) {
        Role created = repository.save(role);

        AuditDetails auditDetails = new AuditDetails(SecurityContextHolder.getContext().getAuthentication().getName(), role.getName());
        try {
            kafkaTemplate.send("jp-topic", mapper.writeValueAsString(auditDetails));
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error parsing the message");
        }
        return repository.save(role);
    }

    public Role updateRole(Role role, Integer roleId) {
        Optional<Role> result = repository.findById(roleId);
        if (result.isPresent()) {
            return repository.save(role);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Role id %d does not exists", roleId));
    }

    public void deleteRole(Integer roleId) {
        Optional<Role> result = repository.findById(roleId);
        if (result.isPresent()) {
            repository.delete(result.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Role id %d does not exists", roleId));
        }
    }
}
