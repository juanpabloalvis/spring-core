package com.springbootrest.services;

import com.springbootrest.models.Address;
import com.springbootrest.models.Profile;
import com.springbootrest.repositories.AddressRepository;
import com.springbootrest.repositories.ProfileRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class AddressService {


    private final AddressRepository addressRepository;
    private final ProfileRepository profileRepository;

    public List<Address> findAddressesByProfileAndUserId(Integer userId, Integer profileId) {
        return addressRepository.findByProfileId(userId, profileId);
    }

    public Address createAddress(Integer userId, Integer profileId, Address address) {
        Optional<Profile> result = profileRepository.findByUserIdAndId(userId, profileId);
        if (result.isPresent()) {
            address.setProfile(result.get());
            return addressRepository.save(address);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format("Profile not found for userId %d and profileId %d.", userId, profileId));

    }
}
