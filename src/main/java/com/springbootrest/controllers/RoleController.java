package com.springbootrest.controllers;

import com.springbootrest.models.Role;
import com.springbootrest.models.User;
import com.springbootrest.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/roles")
public class RoleController {

    private static final Logger log = LoggerFactory.getLogger(RoleController.class);

    private final RoleService roleService;

    @Secured({"ADMIN", "USER"})
    @GetMapping
    public ResponseEntity<List<Role>> getRoles() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("Name [{}],  Principal [{}],  Credentials[{}],  Roles[{}]"
                , authentication.getName()
                , authentication.getPrincipal()
                , authentication.getCredentials()
                , authentication.getAuthorities());
        roleService.getRoles();
        return new ResponseEntity<>(roleService.getRoles(), HttpStatus.OK);
    }

    @GetMapping("/{roleName}/users")
    public ResponseEntity<List<User>> getRolesByRole(@PathVariable("roleName") String roleName) {
        return new ResponseEntity<List<User>>(roleService.getUsersByRole(roleName), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Role> createRole(@RequestBody Role role) {
        return new ResponseEntity<Role>(roleService.createRole(role), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{roleId}")
    public ResponseEntity<Role> updateRole(@PathVariable("roleId") Integer roleId, @RequestBody Role role) {
        return new ResponseEntity<Role>(roleService.updateRole(role, roleId), HttpStatus.OK);
    }

    @DeleteMapping("/{roleId}")
    public ResponseEntity<Void> deleteRole(@PathVariable("roleId") Integer roleId) {
        roleService.deleteRole(roleId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
