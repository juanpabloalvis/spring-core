package com.springbootrest.controllers;

import com.springbootrest.config.JwtTokenUtil;
import com.springbootrest.models.User;
import com.springbootrest.services.UserDetailCustomService;
import com.springbootrest.services.UserService;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
@CrossOrigin(origins = {"http://localhost:3000", "http://127.0.0.1:8080", "http://localhost:8080"})
public class UserController {

    private final UserService userService;

    private final AuthenticationManager authenticationManager;

    private final UserDetailCustomService userDetailCustomService;

    private final JwtTokenUtil jwtTokenUtil;

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @PostMapping(value = "/authenticate")
    public ResponseEntity<String> createAuthenticationToken(@RequestBody User user) {
        log.info("UserResourceImpl : authenticate");
        JSONObject jsonObject = new JSONObject();
        try {

            authentiacte(user.getUsername(), user.getPassword());

            final UserDetails userDetails = userDetailCustomService.loadUserByUsername(user.getUsername());

            final String token = jwtTokenUtil.generateToken(userDetails);

            jsonObject.put("name", user.getUsername());
            jsonObject.put("roles", userDetails.getAuthorities());
            jsonObject.put("token", token);
            return new ResponseEntity<String>(jsonObject.toString(), HttpStatus.OK);

        } catch (JSONException e) {
            try {
                jsonObject.put("exception", e.getMessage());
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNAUTHORIZED);
        }
    }

    private void authentiacte(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new IllegalArgumentException("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new IllegalArgumentException("INVALID_CREDENTIALS", e);
        }
    }


    @GetMapping //Handler methods: HTTP + Recurso
    @Timed("get.users")
    @Operation(summary = "Return a user list")
    public ResponseEntity<Page<User>> getUsers(@RequestParam(required = false, value = "page", defaultValue = "0") int page,
                                               @RequestParam(required = false, value = "size", defaultValue = "100") int size) {
        return new ResponseEntity<Page<User>>(userService.getUsers(page, size), HttpStatus.OK);
    }

    @GetMapping("/usernames") //Handler methods: HTTP + Recurso
    @Operation(summary = "Return username list")
    public ResponseEntity<Page<String>> getUserNames(@RequestParam(required = false, value = "page", defaultValue = "0") int page,
                                                     @RequestParam(required = false, value = "size", defaultValue = "100") int size) {
        return new ResponseEntity<Page<String>>(userService.getUsernames(page, size), HttpStatus.OK);
    }

    @GetMapping(value = "/{userId}")
    @Operation(summary = "Return a user for a given user id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The record was found"),
            @ApiResponse(code = 404, message = "The record was not found")
    })

    public ResponseEntity<User> getUserById(@PathVariable("userId") Integer userId) {
        return new ResponseEntity<User>(userService.getUserById(userId), HttpStatus.OK);
    }

    @GetMapping(value = "/username/{username}")
    public ResponseEntity<User> getUserByName(@PathVariable("username") String username) {
        return new ResponseEntity<User>(userService.getUserByName(username), HttpStatus.OK);
    }

    /**
     * Se hace por post para no enviar el password por la URL,
     *
     * @param user the user
     * @return the user
     */
    @PostMapping("/auth")
    public ResponseEntity<User> getUserByNameAndPassword(@RequestBody User user) {
        return new ResponseEntity<User>(
                userService.getUserByNameAndPassword(user.getUsername(), user.getPassword()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) {
        return new ResponseEntity<User>(userService.createUser(user), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{userId}")
    public ResponseEntity<User> updateUser(@PathVariable("userId") Integer userId, @RequestBody User user) {
        user.setId(userId);
        return new ResponseEntity<User>(userService.updateUser(user, userId), HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable("username") Integer userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
