package com.springbootrest.controllers;

import com.springbootrest.aop.TargetObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // Registra mi app en el contexto de spring
@RequestMapping("/aspect") // Define mi recurso
public class MyAspectController {

    @GetMapping("/1")
    public String first() {
        TargetObject targetObject = new TargetObject();
        targetObject.hello("hola");
        return "Target Object created";
    }

    @GetMapping("/2")
    public String second() {
        TargetObject.helloStatic("hola");
        return "Target Object created";
    }

    @GetMapping("/3")
    public String third() {
        TargetObject.helloStaticWithReturn("hola");
        return "Target Object created";
    }

}
