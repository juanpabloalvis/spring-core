package com.springbootrest.controllers;

import com.springbootrest.models.User;
import com.springbootrest.services.UserServiceUsingList;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/users")
public class V1UserController {

    private final UserServiceUsingList userServiceUsingList;

    @GetMapping //Handler methods: HTTP + Recurso
    public ResponseEntity<Set<User>> getUsers(@RequestParam(value = "startWith", required = false)String startWith) {
        return new ResponseEntity<Set<User>>(userServiceUsingList.getUsers(startWith), HttpStatus.OK);
    }

    @GetMapping(value = "/{username}")
    public ResponseEntity<User> getUserByName(@PathVariable("username") String username) {
        return new ResponseEntity<User>(userServiceUsingList.getUserByName(username), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) {
        return new ResponseEntity<User>(userServiceUsingList.createUser(user), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{username}")
    public ResponseEntity<User> updateUser(@PathVariable("username") String username, @RequestBody User user) {
        return new ResponseEntity<User>(userServiceUsingList.updateUser(user, username), HttpStatus.OK);
    }

    @DeleteMapping("/{username}")
    public ResponseEntity<Void> deleteUser(@PathVariable("username") String username) {
        userServiceUsingList.deleteUser(username);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
