package com.springbootrest.controllers;

import com.springbootrest.models.Post;
import com.springbootrest.services.PostsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@RestController
@RequestMapping("/posts")
public class PostsConsumer {

    private final PostsService postsService;

    @GetMapping
    public Flux<Post> getAllPost() {
        Flux<Post> all = postsService.getAll();

        return all;
    }

    @GetMapping(value = "/{postId}")
    public Mono<Post> getPostById(@PathVariable("postId") String id) {
        Mono<Post> byId = postsService.findById(id);

        return byId.map(post -> post)
                .switchIfEmpty(Mono.error(new Exception("Not found")));

    }

    @PostMapping
    public Mono<Post> createPost(@RequestBody Post post) {
        post.setCreationDate(LocalDateTime.now());
        return postsService.createPost(post);
    }

    @PutMapping
    public ResponseEntity<String> updatePost(Post post) {
        return postsService.updatePost(post);
    }

    @DeleteMapping(value = "/{postId}")
    public Mono<Void> deletePost(@PathVariable("postId") String id) {
        return postsService.deleteById(id);
    }

}
