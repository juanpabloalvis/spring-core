package com.springbootrest.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // Registra mi app en el contexto de spring
@RequestMapping("/hello") // Define mi recurso
public class MyController {

    @GetMapping
    public String helloWorld() {
        return "Hi there!";
    }

}
