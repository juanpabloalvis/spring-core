package com.springbootrest.controllers.openapi;

import com.springrest.api.PetsApi;
import com.springrest.api.model.Error;
import com.springrest.api.model.Pet;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/pet")
//@Tag(name = "Pets", description = "the Pets API")
public class PetsApiController implements PetsApi {


    @GetMapping(value = "/{petId}", produces = {"application/json"})
    @Operation(
            operationId = "dd Return a pet for a given prt id",
            summary = "Info for a specific pet contract firs",
            tags = {"pets"},
            responses = {
                    @io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Expected response to a valid request", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Pet.class))
                    }),
                    @ApiResponse(responseCode = "200", description = "unexpected error", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))
                    })
            }
    )
    public ResponseEntity<Pet> showPetById(@Parameter(name = "petId", description = "The id of the pet to retrieve", required = true)
                                           @PathVariable("petId") String petId) {

        log.info("Request for pet ID: {}", petId);
        Pet pet = new Pet();
        pet.setId(Long.valueOf(petId));
        pet.setName("Good Example");
        pet.setTag("Fake pet using contract first");

        return new ResponseEntity<>(pet, HttpStatus.OK);

    }

}
