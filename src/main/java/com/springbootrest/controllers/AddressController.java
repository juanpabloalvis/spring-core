package com.springbootrest.controllers;

import com.springbootrest.models.Address;
import com.springbootrest.services.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users/{userId}/profiles/{profileId}/addresses")
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public ResponseEntity<List<Address>> findAddressesByProfileAndUserId(@PathVariable("userId") Integer userId, @PathVariable("profileId") Integer profileId) {

        return new ResponseEntity<>(addressService.findAddressesByProfileAndUserId(userId, profileId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Address> create(@PathVariable("userId") Integer userId,
                                          @PathVariable("profileId") Integer profileId,
                                          @RequestBody Address address) {
        return new ResponseEntity<Address>(addressService.createAddress(userId, profileId, address), HttpStatus.CREATED);

    }

    @PostMapping("/healt")
    public ResponseEntity<Map> healt(@PathVariable("userId") Integer userId,
                                     @PathVariable("profileId") Integer format,
                                     @RequestBody Address address) {
        HashMap<String, String> response = new HashMap<>();
        response.put("status", "OK");

        if (format.equals("full")) {
            LocalDateTime now = LocalDateTime.now();
            ZonedDateTime zonedDateTime = now.atZone(ZoneId.systemDefault());
            response.put("currentTime", String.valueOf(zonedDateTime));
            return new ResponseEntity<>(response, HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }

}
