package com.springbootrest.controllers.interceptors;//package com.springbootrest.controllers.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Scanner;


/**
 * TODO validar y hacer esta implementacion de Generic filter INterceptor
 */
@Component
@Order(3)
@Slf4j
public class GenericFilterInterceptor extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        String ipAddress = httpServletRequest.getHeader("X-FORWARDED-FOR") == null
                ? httpServletRequest.getRemoteAddr()
                : httpServletRequest.getHeader("X-FORWARDED-FOR");

        logger.info("Inside Servlet Filter");
        logger.info(String.format("User IP address: [%s]", ipAddress));
        logger.info(String.format("Request Params: [%s]", getParams(httpServletRequest)));

//        logger.info(String.format("Request Payload: [%s]", getPayload(httpServletRequest)));
        logger.info("Exiting Servlet Filter");
        filterChain.doFilter(httpServletRequest, servletResponse);
    }

    private String getParams(HttpServletRequest request) {
        final StringBuilder params = new StringBuilder();

        Enumeration<String> parameterNames = request.getParameterNames();

        for (; parameterNames.hasMoreElements(); ) {
            String paramName = parameterNames.nextElement();
            String paramValue = request.getParameter(paramName);
            if ("password".equalsIgnoreCase(paramName) || "pwd".equalsIgnoreCase(paramName)) {
                paramValue = "*****";
            }
            params.append(paramName).append(": ").append(paramValue).append(System.lineSeparator());
        }
        return params.toString();
    }

    private String getPayload(HttpServletRequest httpServletRequest) {

        if ("POST".equalsIgnoreCase(httpServletRequest.getMethod())) {
            Scanner s = null;
            try {
                s = new Scanner(httpServletRequest.getInputStream(), "UTF-8").useDelimiter("\\A");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }
}
