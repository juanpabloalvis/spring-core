//package com.springbootrest.controllers.interceptors;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//import java.util.Enumeration;
//import java.util.Scanner;
//
//@Component
//@Order(3)
//@Slf4j
//public class FilterInterceptor implements Filter {
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//
//        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
//
//        String ipAddress = httpServletRequest.getHeader("X-FORWARDED-FOR") == null
//                ? httpServletRequest.getRemoteAddr()
//                : httpServletRequest.getHeader("X-FORWARDED-FOR");
//
//        log.info("Inside Servlet Filter");
//        log.info("User IP address: [{}]", ipAddress);
//        log.info("Request Params: [{}]", getParams(httpServletRequest));
//
//        log.info("Request Payload: [{}]", getPayload(httpServletRequest));
//        log.info("Exiting Servlet Filter");
//        filterChain.doFilter(httpServletRequest, servletResponse);
//    }
//
//    private String getParams(HttpServletRequest request) {
//        final StringBuilder params = new StringBuilder();
//
//        Enumeration<String> parameterNames = request.getParameterNames();
//
//        for (; parameterNames.hasMoreElements(); ) {
//            String paramName = parameterNames.nextElement();
//            String paramValue = request.getParameter(paramName);
//            if ("password".equalsIgnoreCase(paramName) || "pwd".equalsIgnoreCase(paramName)) {
//                paramValue = "*****";
//            }
//            params.append(paramName).append(": ").append(paramValue).append(System.lineSeparator());
//        }
//        return params.toString();
//    }
//
//    private String getPayload(HttpServletRequest httpServletRequest) {
//
//        if ("POST".equalsIgnoreCase(httpServletRequest.getMethod())) {
//            Scanner s = null;
//            try {
//                s = new Scanner(httpServletRequest.getInputStream(), "UTF-8").useDelimiter("\\A");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return s.hasNext() ? s.next() : "";
//        }
//        return "";
//    }
//}
