//package com.springbootrest.controllers.interceptors;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//
//public class InterceptorAppConfig extends WebMvcConfigurationSupport {
//    @Autowired
//    GeneralInterceptor generalInterceptor;
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(generalInterceptor);
//    }
//}
