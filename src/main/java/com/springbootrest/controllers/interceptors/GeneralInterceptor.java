//package com.springbootrest.controllers.interceptors;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Enumeration;
//import java.util.Scanner;
//
//@Slf4j
//@Component
//public class GeneralInterceptor implements HandlerInterceptor {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        log.info("GeneralInterceptor preHandleInterceptor");
//        log.info("GeneralInterceptor Inside Servlet Filter");
//        log.info("GeneralInterceptor User IP address: [{}]", request.getRemoteAddr());
//        log.info("GeneralInterceptor Request Params: [{}]", getParams(request));
//
//        log.info("GeneralInterceptor Request Payload: [{}]", getPayload(request));
//        log.info("GeneralInterceptor Exiting Servlet Filter");
//        return true;
//    }
//
//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        log.info("Post handle method has been called ");
//
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        log.info("A fterCompletion method has been called");
//
//    }
//
//
//    private String getParams(HttpServletRequest request) {
//        final StringBuilder params = new StringBuilder();
//
//        Enumeration<String> parameterNames = request.getParameterNames();
//
//        for (; parameterNames.hasMoreElements(); ) {
//            String paramName = parameterNames.nextElement();
//            String paramValue = request.getParameter(paramName);
//            if ("password".equalsIgnoreCase(paramName) || "pwd".equalsIgnoreCase(paramName)) {
//                paramValue = "*****";
//            }
//            params.append(paramName).append(": ").append(paramValue).append(System.lineSeparator());
//        }
//        return params.toString();
//    }
//
//    private String getPayload(HttpServletRequest httpServletRequest) {
//
//        if ("POST".equalsIgnoreCase(httpServletRequest.getMethod())) {
//            Scanner s = null;
//            try {
//                s = new Scanner(httpServletRequest.getInputStream(), "UTF-8").useDelimiter("\\A");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return s.hasNext() ? s.next() : "";
//        }
//        return "";
//    }
//}
