package com.springbootrest.controllers;

import com.springbootrest.models.MyCustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // Registra mi app en el contexto de spring
@RequestMapping("/intercept") // Define mi recurso
@Slf4j
public class ToBeInterceptedController {

    @GetMapping("/with/filter") //intercept/with/filter
    public String intercepted() {
        return "Hi, I was intercepted with Filter interceptor, isn't it?";
    }

    @GetMapping("/with/interceptor")
    public String handlerInterceptor() {
        return "Hi, I was intercepted with HandlerInterceptor, isn't it?";
    }

    @GetMapping("/exception")
    public String errorHelloWorld() {
        log.info("error i controller was called");
        throw new MyCustomException();
    }
}
