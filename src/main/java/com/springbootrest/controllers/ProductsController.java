package com.springbootrest.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/product")
public class ProductsController {

    private final Environment environment;

    @GetMapping
    public ResponseEntity<String> getProduct() {
        return new ResponseEntity<>(String.format("HTTP Get Handled in port %s", getPort()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> createProduct() {
        return new ResponseEntity<>(String.format("HTTP POST Handled in port %s", getPort()), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<String> updateProduct() {
        return new ResponseEntity<>(String.format("HTTP PUT Handled in port %s", getPort()), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteProduct() {
        return new ResponseEntity<>(String.format("HTTP DELETE Handled in port %s", getPort()), HttpStatus.OK);
    }

    private String getPort() {
        return environment.getProperty("local.server.port");
    }

}
