package com.springbootrest.controllers;

import com.springbootrest.models.MyCustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // Registra mi app en el contexto de spring
@RequestMapping("/error") // Define mi recurso
//@Slf4j
public class ErrorController {
    @GetMapping("/exception")
    public String errorHelloWorld() {
//        log.info("error controller was called");
        throw new MyCustomException();
    }
}
