package com.springbootrest.controllers;

import com.springbootrest.models.Profile;
import com.springbootrest.services.ProfileService;
import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users/{userId}/profiles")
public class ProfileController {

    private final ProfileService service;


    @Timed("get.profile")
    @GetMapping("/{profileId}")
    @Operation(summary = "Return a profile list")
    public ResponseEntity<Profile> getProfile(@PathVariable("userId") Integer userId, @PathVariable("profileId") Integer profileId) {
        return new ResponseEntity<Profile>(service.getByUserIdAndProfileId(userId, profileId), HttpStatus.OK);
    }

    @Timed("post.profile")
    @PostMapping
    @Operation(summary = "Return a user list")
    public ResponseEntity<Profile> createProfile(@PathVariable("userId") Integer userId, @RequestBody Profile profile) {
        return new ResponseEntity<Profile>(service.createProfile(userId, profile), HttpStatus.OK);
    }

    @PutMapping(value = "/{userId}")
    public ResponseEntity<Profile> updateUser(@PathVariable("userId") Integer userId, @RequestBody Profile profile) {
        return new ResponseEntity<Profile>(service.updateProfile(userId, profile), HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable("userId") Integer userId, @PathVariable("profileId") Integer profileId) {
        service.deleteProfile(userId, profileId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
