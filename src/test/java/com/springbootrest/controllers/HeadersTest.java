package com.springbootrest.controllers;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;

import java.util.Map;


@ActiveProfiles("test")
public class HeadersTest {



    @Test
    public void headersTestSingle() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-b3-spanid", "3e20578e");
        headers.add("x-b3-traceid", "3e20578e");
        Map<String, String> stringStringMap = headers.toSingleValueMap();
        System.out.println(stringStringMap.size());
    }
}