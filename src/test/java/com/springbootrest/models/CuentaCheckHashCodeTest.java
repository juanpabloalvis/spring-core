package com.springbootrest.models;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CuentaCheckHashCodeTest {

    @Test
    void test_Nombre_Cuenta() {
        Cuenta cuenta = Cuenta.builder()
                .persona("Juan")
                .saldo(new BigDecimal("25000"))
                .build();
        String esperado = "Juan";

        assertEquals(esperado, cuenta.getPersona());

    }

    @Test
    void test_Saldo_Cuenta() {
        Cuenta cuenta = Cuenta.builder()
                .persona("Juan")
                .saldo(new BigDecimal("25000"))
                .build();
        BigDecimal saldoEsperado = new BigDecimal("25000");

        assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);

    }
    @Test
    void test_Referencia_Cuenta() {
        Cuenta cuenta = Cuenta.builder()
                .persona("Juan")
                .saldo(new BigDecimal("25000"))
                .build();
        Cuenta cuenta2 = Cuenta.builder()
                .persona("Juan")
                .saldo(new BigDecimal("25000"))
                .build();

        // assertNotEquals(cuenta, cuenta2);

    }
}