package com.lambda;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;

import static com.lambda.FunctionalPatternCombinator.CustomerRegistrationValidatorCombinator.*;
import static com.lambda.FunctionalPatternCombinator.CustomerRegistrationValidatorCombinator.ValidationResult.*;


class FunctionalPatternCombinator {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void combinator() {
        Customer joao = Customer.builder()
                .name("Joao")
                .email("juanpabloalvis@123.com")
                .dateOfBirthday(LocalDate.of(1991, 5, 17))
                .phoneNumber("+099999").build();

        // old school
        System.out.println("Is joao customer valid: " + new CustomerValidatorService().isCustomerValid(joao));

        // new school, using combinator pattern
        ValidationResult validationResult = isAdult()
                .and(isEmailValid())
                .and(isPhoneNumberValid())
                .apply(joao);
        if (!SUCCESS.equals(validationResult)) {
            throw new IllegalArgumentException(" Is not success: " + validationResult.name());
        }

        // we can store customer in db

    }

    // model
    @ToString
    @Builder
    @RequiredArgsConstructor
    private static class Customer {
        private final String name;
        private final String email;
        private final String phoneNumber;
        private final LocalDate dateOfBirthday;

    }

    // imperative
    // service
    private static class CustomerValidatorService {

        private boolean isEmailValid(String email) {
            return email.contains("@");
        }

        private boolean isPhoneNumberValid(String phoneNumber) {
            return phoneNumber.startsWith("+0");
        }

        private boolean isAdult(LocalDate dob) {
            return Period.between(LocalDate.now(), dob).getYears() > 18;
        }

        public boolean isCustomerValid(Customer customer) {
            return isEmailValid(customer.email)
                    && isPhoneNumberValid(customer.phoneNumber)
                    && isAdult(customer.dateOfBirthday);
        }
    }

    // declarative
    public interface CustomerRegistrationValidatorCombinator extends Function<Customer, ValidationResult> {
        static CustomerRegistrationValidatorCombinator isEmailValid() {
            return customer -> customer.email.contains("@") ? SUCCESS : EMAIL_NOT_VALID;
        }

        static CustomerRegistrationValidatorCombinator isPhoneNumberValid() {
            return customer -> customer.phoneNumber.startsWith("+0") ? SUCCESS : PHONE_NUMBER_NOT_VALID;
        }

        static CustomerRegistrationValidatorCombinator isAdult() {
            return customer -> Period.between(customer.dateOfBirthday, LocalDate.now()).getYears() > 18 ? SUCCESS : IS_NOT_AN_ADULT;
        }

        default CustomerRegistrationValidatorCombinator and(CustomerRegistrationValidatorCombinator other) {
            return customer -> {
                ValidationResult result = this.apply(customer);
                return result.equals(SUCCESS) ? other.apply(customer) : result;
            };
        }

        enum ValidationResult {
            SUCCESS,
            PHONE_NUMBER_NOT_VALID,
            EMAIL_NOT_VALID,
            IS_NOT_AN_ADULT
        }
    }

}