package com.lambda;

import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

class FunctionalInterfaceSuplier {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void supplier() {

        System.out.println(getDbConnectionURL());

        // the purpose to return
        System.out.println(getDbConnectionSupplier.get());

        // custom Supplier with args
        System.out.println(getDbConnectionEnvSupplier("dev").get());
        System.out.println(getDbConnectionEnvSupplier("local").get());

    }

    // imperative
    static String getDbConnectionURL() {
        return "jdbc://localhost:5432/users";
    }

    // declarative
    // we can return whatever we want, custom clases, custom objects, an array, a list
    Supplier<String> getDbConnectionSupplier = () -> "jdbc://localhost:5432/users";

    Supplier<String> getDbConnectionEnvSupplier(String environment) {
        if (environment.equals("dev")) {
            return () -> "jdbc://dev:5432/users";
        }
        if (environment.equals("prod")) {
            return () -> "jdbc://production:5432/users";
        }
        if (environment.equals("local")) {
            return () -> "jdbc://localhost:5432/users";
        }
        throw new IllegalArgumentException(environment + "Is invalid environment");
    }

}