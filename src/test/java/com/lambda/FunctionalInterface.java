package com.lambda;

import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

class FunctionalInterface {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void listing() {
        int increment = increment(0);
        System.out.println(increment);

        Integer increment2 = incrementByOneFunction.apply(increment);
        System.out.println(increment2);

        // other function
        Integer multiply = multiplyBy10.apply(increment2);
        System.out.println(multiply);

        // or both at the same time
        Function<Integer, Integer> addOneAndThenMultiplyBy10 = incrementByOneFunction
                .andThen(multiplyBy10);
        System.out.println(addOneAndThenMultiplyBy10.apply(1));

        // bifucntion, takes two arguments and return one
        Integer addOneAndMultiplyBy = addOneAndMultiply.apply(1, 10);
        System.out.println(addOneAndMultiplyBy);

    }

    // function that increment a number by one
    int increment(int number) {
        return number + 1;

    }

    // Functional style, the same as before method. Interface Function<T,R> (básicamente toma el input(T) y el output(R)
    Function<Integer, Integer> incrementByOneFunction = number -> number + 1;

    Function<Integer, Integer> multiplyBy10 = number -> number * 10;

    // takes two args and return the 3 type
    BiFunction<Integer, Integer, Integer> addOneAndMultiply =
            (numberToAddOne, multiplyByNumber) -> (numberToAddOne + 1) * multiplyByNumber;
}