package com.lambda;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

class FunctionalInterfaceConsumer {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void consumer() {

        Customer joao = Customer.builder().name("Joao").phoneNumber("899999").build();
        // consumer represents an operation that accepts a single input argument and return no result argument
        // using imperative function
        greet(joao);

        // declarative
        greetCustomerConsumer.accept(joao);
        greetBiCustomerConsumer.accept(joao, true);
        greetBiCustomerConsumer.accept(joao, false);

    }

    // imperative
    static void greet(Customer customer) {
        System.out.println("Hello ".concat(customer.name.concat(" - ").concat(customer.phoneNumber)));
    }

    // declarative
    Consumer<Customer> greetCustomerConsumer = customer -> System.out.println("Hello ".concat(customer.name.concat(" - ").concat(customer.phoneNumber)));

    BiConsumer<Customer, Boolean> greetBiCustomerConsumer =
            (customer, showPhoneNumber) -> System.out.println(
                    "Hello ".concat(customer.name)
                            .concat(showPhoneNumber ? " - ".concat(customer.phoneNumber) : "*********")

            );

    @ToString
    @Builder
    @RequiredArgsConstructor
    private static class Customer {
        private final String name;
        private final String phoneNumber;

    }

}