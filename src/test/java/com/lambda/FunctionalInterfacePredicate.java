package com.lambda;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

class FunctionalInterfacePredicate {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void predicate() {

        String phoneNumber = "07000010000";
        String phoneNumber2 = "10000010000";
        String phoneNumber3 = "1070004";
        String phoneNumber4 = "07000010003";
        String phoneNumber5 = "0003";

        System.out.println("Without Predicate");
        System.out.println(isPhoneNumberValid(phoneNumber));
        System.out.println(isPhoneNumberValid(phoneNumber2));
        System.out.println(isPhoneNumberValid(phoneNumber3));

        // Represents a predicate (boolean-value function) of one argument
        System.out.println("With Predicate");
        System.out.println(isPhoneNumberPredicateValid.test(phoneNumber));
        System.out.println(isPhoneNumberPredicateValid.test(phoneNumber2));
        System.out.println(isPhoneNumberPredicateValid.test(phoneNumber3));
        // nested predicate
        System.out.println(
                "Is phone number valid and contains 3: " +
                        isPhoneNumberPredicateValid.and(containsNumber3)
                                .test(phoneNumber4));
        System.out.println(
                "Is phone number valid or contains 3: " +
                        isPhoneNumberPredicateValid.or(containsNumber3)
                                .test(phoneNumber5));

        // BiPredicate
        System.out.println("BiPredicate: ");
        System.out.println("areEquals: " + areEqualValues.test("345", 345));
        System.out.println("areEquals" + areEqualValues.test("345", 384));

    }

    // imperative
    static boolean isPhoneNumberValid(String phoneNumber) {
        // simple validation
        return phoneNumber.startsWith("07") && phoneNumber.length() == 11;
    }

    // declarative
    Predicate<String> isPhoneNumberPredicateValid = (phoneNumber) -> phoneNumber.startsWith("07") && phoneNumber.length() == 11;
    Predicate<String> containsNumber3 = (phoneNumber) -> phoneNumber.contains("3");

    BiPredicate<String, Integer> areEqualValues = (numberStr, number) -> NumberUtils.isParsable(numberStr) && (NumberUtils.toInt(numberStr) == number);


}