package com.lambda;

import org.junit.jupiter.api.Test;

import java.util.function.Consumer;


class FunctionalPatternLikeJSCallback {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void callback() {
        /*
         *
         function hello(firstName, lastName, callback){
            console.log(firstName);
            if(lastName){
                console.log(lastName)
            }else{
                callback();
            }
         }

         > hello("Juan", "alvis")
         > hello("hola", null, function() {console.log("No lastName provided")}
         > hello("hola", "ddddd", function() {console.log("No lastName provided")} // the callback is not reached
         */

        // Using Consumer
        hello("Juan", "alvis", null);
        hello("Juan",
                null,
                value -> System.out.println("Last name was not provided for " + value));

        // Using runnable
        hello2("Juan", "alvis", null);
        hello2("Juan",
                null,
                () -> System.out.println("Last name was not provided "));
    }

    void hello(String firstName, String lastName, Consumer<String> callback) {
        System.out.println(firstName);


        //  Optional.ofNullable(lastName).ifPresentOrElse(System.out::println, () -> callback.accept(firstName));
        if (lastName != null) {
            System.out.println(lastName);

        } else {
            // this callback, that does ir return the value "firstName" to the call function
            callback.accept(firstName);
        }
    }

    void hello2(String firstName, String lastName, Runnable callback) {
        System.out.println(firstName);


        //  Optional.ofNullable(lastName).ifPresentOrElse(System.out::println, () -> callback.accept(firstName));
        if (lastName != null) {
            System.out.println(lastName);

        } else {
            callback.run();
        }
    }

    // imperative

    // declarative

}