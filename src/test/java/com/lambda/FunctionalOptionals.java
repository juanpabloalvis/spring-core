package com.lambda;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.function.Supplier;


class FunctionalOptionals {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void optionals() {
        // it helps to deal with NPE
        Object value = Optional.ofNullable(null)
                .orElseGet(() -> "Defautl value");
        System.out.println(value);

        Optional.ofNullable(null)
                .ifPresent(System.out::println);
        Optional.ofNullable("juanpabloalvis@yahoo.com")
                .ifPresent(email -> {
                    // logic
                    System.out.println("Sending an email to " + email);
                });
        Optional.ofNullable("juanpabloalvis@yahoo.com")
                .ifPresentOrElse(email -> System.out.println("Sending an email to " + email),
                        () -> System.out.println("Cannot send email."));


        Supplier<IllegalArgumentException> exception = () -> new IllegalArgumentException("Exception");
//        Object value2 = Optional.ofNullable(null)
//                .orElseThrow(exception);
    }
}