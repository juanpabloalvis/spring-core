package com.lambda;

import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;


class FunctionalTrueLambdas {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void lambdas() {


        System.out.println(upperCase.apply("Juan Pablo Alvis C"));
        System.out.println(upperCaseNameAndAge.apply("Juan Pablo Alvis C", 35));
        System.out.println(FunctionalTrueLambdas.staticUpperCaseNameAndAge
                .apply("Juan Pablo Alvis C", 35));


    }

    // declarative
//   ...<inputType, returnType>
    //        Function<String, String> upperCase = String::toUpperCase;
    Function<String, String> upperCase = str -> {
        if (str.isBlank()) throw new IllegalArgumentException("Empty String");
        return str.toUpperCase();
    };

    BiFunction<String, Integer, String> upperCaseNameAndAge = (name, age) -> {
        if (name.isBlank()) throw new IllegalArgumentException("Empty String");
        return name.toUpperCase() + " has " + age + " years old ";
    };

    static BiFunction<String, Integer, String> staticUpperCaseNameAndAge = (name, age) -> {
        if (name.isBlank()) throw new IllegalArgumentException("Empty String");
        return name.toUpperCase() + " has " + age + " years old ";
    };

}