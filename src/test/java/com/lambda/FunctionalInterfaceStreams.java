package com.lambda;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static com.lambda.FunctionalInterfaceStreams.Gender.FEMALE;
import static com.lambda.FunctionalInterfaceStreams.Gender.MALE;


class FunctionalInterfaceStreams {

    //    https://download.java.net/java/early_access/loom/docs/api/java.base/java/util/function/package-summary.html

    @Test
    void streams() {
        List<Person> people = List.of(
                Person.builder().name("Juan").gender(MALE).build(),
                Person.builder().name("Pablo").gender(MALE).build(),
                Person.builder().name("Aisha").gender(FEMALE).build(),
                Person.builder().name("Rut").gender(FEMALE).build(),
                Person.builder().name("Carlos").gender(MALE).build(),
                Person.builder().name("Alice").gender(FEMALE).build(),
                Person.builder().name("Nick").gender(MALE).build(),
                Person.builder().name("María").gender(FEMALE).build()
        );

        Set<Gender> genders = people.stream()
                .map(person -> person.gender)
                .collect(Collectors.toSet());

        System.out.println("Print unique genders");
        genders.forEach(System.out::println); // this was replaced with method reference System.out.println(gender);

        // simmilar but get unique names then printed
        System.out.println("Print unique names");
        people.stream()
                .map(person -> person.name)
                .collect(Collectors.toSet())
                .forEach(System.out::println);

        // simmilar, map person name and print each length
        System.out.println("Print length of each string ");
        ToIntFunction<String> length = String::length;
        // input Person, return String:
        Function<Person, String> personStringFunction = person -> person.name;
        // consumer no returns
        IntConsumer consumer = System.out::println;
        people.stream()
                .map(personStringFunction)
                .mapToInt(length)
                .forEach(consumer);

        //
        Predicate<Person> containsFemalesAndMalesPredicate = person -> FEMALE.equals(person.gender) || MALE.equals(person.gender) ;
        boolean containsOnlyMales = people.stream()
                .allMatch(containsFemalesAndMalesPredicate);
        Predicate<Person> containsFemalesPredicate = person -> FEMALE.equals(person.gender) ;
        System.out.println("Contains females and males?");
        System.out.println(containsOnlyMales);
        boolean containsAnyMales = people.stream()
                .anyMatch(containsFemalesPredicate);
        System.out.println("Contains any females?");
        System.out.println(containsAnyMales);


    }

    // imperative

    // declarative


    @ToString
    @RequiredArgsConstructor
    @Builder
    private static class Person {
        private final String name;
        private final Gender gender;
    }

    enum Gender {
        MALE, FEMALE
    }
}