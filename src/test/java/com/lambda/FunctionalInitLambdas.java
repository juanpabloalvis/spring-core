package com.lambda;

import lombok.Builder;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.lambda.FunctionalInitLambdas.Gender.FEMALE;
import static com.lambda.FunctionalInitLambdas.Gender.MALE;

class FunctionalInitLambdas {


    @Test
    void listing() {
//        ======================= RULES =======================
//        - No State : No depends of other state, should not depend on global or other state. ( for example, here no depends on counter: com/lambda/FunctionalTrueLambdas.java:34)
//        - Pure Functions
//        - No side Effects : Should not have effect outside of the function
//        - Higher order functions
//        =======================
//              * the function takes one or more functions as parameters (whe accept a function as parameter com/lambda/FunctionalPatternLikeJSCallback.java:43)
//              * the function return another function as result (here we return a function-interface that extends from function-com/lambda/FunctionalPatternCombinator.java:85)
//        ======================= RULES =======================
        List<Person> people = List.of(
                Person.builder().name("Juan").gender(MALE).build(),
                Person.builder().name("Pablo").gender(MALE).build(),
                Person.builder().name("Aisha").gender(FEMALE).build(),
                Person.builder().name("Rut").gender(FEMALE).build(),
                Person.builder().name("Carlos").gender(MALE).build(),
                Person.builder().name("Alice").gender(FEMALE).build(),
                Person.builder().name("Nick").gender(MALE).build(),
                Person.builder().name("María").gender(FEMALE).build()
        );

        //          Imperatirve
        List<Person> females = new ArrayList<>();
        for (Person person : people) {
            if (FEMALE.equals(person.gender)) {
                females.add(person);
            }
        }
        for(Person person : females){  System.out.println(person);     }

        //          Declarative
        Predicate<Person> personPredicate = person -> MALE.equals(person.gender);
        people.stream()
                .filter(personPredicate)
                .collect(Collectors.toList()) // this line is not necessary
                .forEach(System.out::println);
    }

    @ToString
    @Builder
    private static class Person {
        private final String name;
        private final Gender gender;

        public Person(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }

    }

    enum Gender {
        MALE, FEMALE
    }
}